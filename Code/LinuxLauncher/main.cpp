/*************************************************************************
Copyright (C), RadFox Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 20/01/2019  14:25 : Created by Stanislav Migunov
*************************************************************************/

#include <Engine/enginecore.h>
#include <Engine/enginecore_impl.inl>
#include <RFLibrary.h>
#include <Startup/GameStartup.h>

RadDynamic rdGameDll = nullptr;

int RunGame(const char* commandLine)
{	
	rdGameDll = RadLoadLibrary(Library_Game);

	if(!rdGameDll)
	{
		RadMessageBox("ERROR", "Failed to load the Game DLL!", eBT_Error);
		return 0;
	}

	IGameStartup::TEntryPoint epCreateGameStartup = RadProcAddress<IGameStartup::TEntryPoint>(rdGameDll, "CreateGameStartup");
	if(!epCreateGameStartup)
	{
		RadLibraryFree(rdGameDll);
		RadMessageBox("ERROR", "Failed to load the Game DLL!", eBT_Error);
		return 0;
	}

	CGameStartup* pGameStartup = (CGameStartup*)epCreateGameStartup();
	if (!pGameStartup)
	{
		RadLibraryFree(rdGameDll);
		RadMessageBox("ERROR", "Creation of the Startup Interface failed", eBT_Error);
		return 0;
	}

	SInitializeParams params;
	params.rootDir = "";
	params.logFileName = "Engine";

	if (pGameStartup->Initialize(params))
	{
		//Call Run to start the game loop.
		pGameStartup->Run();

		//Shutdown after game is finished.
		pGameStartup->Shutdown();
		pGameStartup = nullptr;
		RadLibraryFree(rdGameDll);
	}
	else
	{
		RadMessageBox("ERROR", "Game Startup failed!", eBT_Error);
		pGameStartup->Shutdown();
		pGameStartup = nullptr;
		RadLibraryFree(rdGameDll);
		return 0;
	}

	return 0;
}

int main()
{
	return RunGame(0);
}