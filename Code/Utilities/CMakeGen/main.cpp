/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 13/02/2019  04:29 : Created by Stanislav Migunov
*************************************************************************/

#include <iostream>
#include <conio.h>
#include <string>
#include <Windows.h>

void EngineRoot(std::string& path)
{
	WCHAR szExePathName[_MAX_PATH];
	size_t nLen = GetModuleFileNameW(GetModuleHandle(NULL), szExePathName, _MAX_PATH);

	// Find path above exe name and deepest folder.
	int nCount = 0;
	for (size_t n = nLen - 1; n > 0; n--)
	{
		if (szExePathName[n] == '\\')
		{
			nLen = n;
			if (++nCount == 1)
				break;
		}
	}

	if (nCount > 0)
	{
		szExePathName[nLen++] = 0;

		// Switch to upper folder.
		SetCurrentDirectoryW(szExePathName);
	}
	char* buff = (char*)malloc(_MAX_PATH);
	GetCurrentDirectory(_MAX_PATH, buff);

	path = buff;
}

void ExecSystemCmd(const char* cmd)
{
	system(cmd);
}

int main()
{
	std::cout << "-----------------------------------------------";
	std::cout << "CMAKE SOLUTION GENERATION";
	std::cout << "-----------------------------------------------";
	std::cout << std::endl;

	std::string enginePath;
	
	EngineRoot(enginePath);

	if (enginePath.empty())
	{
		MessageBoxA(NULL, "Engine Root Path not found...", "Error.", MB_OK | MB_ICONERROR);
		return EXIT_FAILURE;
	}
	
	std::cout << "Creating solution folder..." << std::endl;
	std::cout << std::endl;

	ExecSystemCmd("mkdir solution");
	ExecSystemCmd("set \"curpath = %cd%\" ");
	ExecSystemCmd("cd solution");

	ExecSystemCmd(std::string(
		"cmake -G \"Visual Studio 16 2019\" "
		"-S " + enginePath + " "
		"-B " + enginePath + "/solution"
	).c_str());

	std::cout << std::endl << "Copy pdcurses.dll" << std::endl;
	ExecSystemCmd("mkdir _x64_\\Debug");
	ExecSystemCmd("copy /Y Code\\SDK\\pdcurses\\pdcurses.dll _x64_\\Debug");

	std::cout << std::endl;
	std::cout << "Press ENTER to open Project in Visual Studio." << std::endl;

	_getch();

	ExecSystemCmd("cmake --open solution/");

	return EXIT_SUCCESS;
}