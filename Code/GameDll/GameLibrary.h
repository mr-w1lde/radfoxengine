/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 02/02/2018  18:37 : Created by Stanislav Migunov
*************************************************************************/
#include <UnkownLibrary/IUnknownLibrary.h>
#include <RFLibrary.h>
#include <Core/IEngineCore.h>

#ifndef _GAME_LIBRARY_H__
#define _GAME_LIBRARY_H__

class CGameLibrary 
	: public IUnknownLibrary
	, public IEngineEventListener
{
public:
	CGameLibrary() {}
	virtual ~CGameLibrary();

public:
	// IUnknownLibrary
	virtual void            Initialize() override;
	virtual const char*     GetLibraryName() override { return "GameDll"; };
	virtual void			Release() override;
	//~IUnknownLibrary
public:
	// IEngineEventListener
	virtual void OnEngineEvent(const EEngineEvents& event) override;
	//~IEngineEventListener
};

REGISTER_UNKNOWN_LIB(CGameLibrary);

#endif