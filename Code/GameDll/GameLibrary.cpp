/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 02/02/2018  18:37 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "GameLibrary.h"
#include <Core/Console/IConsole.h>
#include <Input/IInput.h>

void RegisterCVars()
{
	gEngine->pConsole->RegisterSection("Game");

	REGISTER_CVAR_STRING(SCVarId("Game", "Title"), "Game", "null");
}


CGameLibrary::~CGameLibrary()
{
}

void CGameLibrary::Initialize()
{
	gEngine->pCore->GetIEngineEvent()->AddListener(this);
}

void CGameLibrary::Release()
{
	gEngine->pLog->Log("[%s] Releasing...", GetLibraryName());
	gEngine->pCore->GetIEngineEvent()->RemoveListener(this);

	this->~CGameLibrary();
}

void CGameLibrary::OnEngineEvent(const EEngineEvents & event)
{
	if (event == EVENT_ENGINE_LIBS_LOADED)
	{
		//Register CVars.
		RegisterCVars();
	}
}
