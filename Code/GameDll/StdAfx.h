/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 22/12/2018  20:39 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _GAME_STDADX_H__
#define _GAME_STDADX_H__

#define ENGINE_API_EXPORT

#include <Engine/enginecore.h>
#include <Core/ILogSystem.h>

#define MODULE_TITLE "[GAME]"

#define GAME_API RADCORE_API

#endif
