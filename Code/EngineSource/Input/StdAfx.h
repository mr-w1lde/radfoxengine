/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 01/02/2019  17:18 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _INPUT_STDADX_H__
#define _INPUT_STDADX_H__

#define ENGINE_API_EXPORT

#include <Engine/enginecore.h>
#include <Core/ILogSystem.h>

#define MODULE_TITILE "[Input] "

#define InputLog(...) do {  gEngine->pLog->Log(MODULE_TITILE __VA_ARGS__); } while(0)
#define InputLogWarngin(...) do {  gEngine->pLog->LogWarning(MODULE_TITILE __VA_ARGS__); } while(0)
#define InputLogError(...) do {  gEngine->pLog->LogError(MODULE_TITILE __VA_ARGS__); } while(0)
#define InputLogSuccess(...) do {  gEngine->pLog->LogSuccess(MODULE_TITILE __VA_ARGS__); } while(0)

#define FATAL_ERROR(x) { RadMessageBox("FATAL ERROR", x, eBT_Error); }

#endif
