/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 13/02/2019  22:34 : Created by Shcherbakov Michail
*************************************************************************/
#include "StdAfx.h"
#include "Input.h"


CInput::CInput()
{
}

CInput::~CInput()
{
	gEngine->pInput->RemoveListener(m_pPlayerInput);
}

void CInput::Initialize()
{
	m_pPlayerInput = new CPlayerInput;
	m_pPlayerInput->Initialize();

	gEngine->pInput->AddListener(m_pPlayerInput);
}

void CInput::Release()
{
}

void CInput::Update()
{
}

void CInput::AddListener(IInputActionListener* listener)
{
	m_ObjectInput.push_back(listener);
}

void CInput::RemoveListener(IInputActionListener* listener)
{
	find_and_erase(m_ObjectInput, listener);
}

void CInput::GetKeyFromGLWF(int key, int action)
{

	for (const auto& it : m_ObjectInput)
	{
		it->OnAction(key, action, 1.f);
	}
}

