/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 13/02/2019  22:34 : Created by Shcherbakov Michail
*************************************************************************/
#include <glfw/include/glfw3.h>
#include <Core/Parsers/IEngineIniParser.h>
#include <Input/IInput.h>

#ifndef _CINPUT_H_
#define _CINPUT_H_

class CPlayerInput;

class CInput : public IInput
{
public:
	CInput();
	~CInput();

public:
	// IInput
	virtual void Initialize() override;
	virtual void Release() override;
	virtual void Update() override;

	virtual void AddListener(IInputActionListener* listener) override;
	virtual void RemoveListener(IInputActionListener* listener) override;
	// ~IInput

public:
	void GetKeyFromGLWF(int key, int action);

private:
	std::vector<IInputActionListener*> m_ObjectInput; 
	CPlayerInput* m_pPlayerInput;

};

/*
* Just for example
*/
class CPlayerInput : public IInputActionListener
{
public:
	CPlayerInput() {}
	~CPlayerInput() {}

public:
	void Initialize()
	{
		m_ActionInputs.LoadFile("GameData/GameInputKeys.ini");

		m_ActionInputs.AvailableSection("Player", false);

		m_ActionInputs.AddHandler("Player", "move_forward", &CPlayerInput::OnMoveForward);
	}

public:
	// IInputActionListener
	virtual void OnAction(int key, int state, float value) override
	{
		m_ActionInputs.Action(key, state, value);
	}
	// ~IInputActionListener

public:
	bool OnMoveForward(int state, float value) 
	{
		if (state == eIS_OnPressed)
		{
			// button is pressed
		}
		else if (state == eIS_OnHold)
		{
			// button is holding
		}
		else 
		{
			// button is released
		}
		return 1; 
	}

private:
	IActionHandler<CPlayerInput> m_ActionInputs;
};


#endif
