/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 13/02/2019  22:34 : Created by Shcherbakov Michail
*************************************************************************/
#ifndef _INTPUT_LABRARY_H_
#define _INTPUT_LABRARY_H_

#include "StdAfx.h"
#include <UnkownLibrary/IUnknownLibrary.h>

class CInput;

class InputLibrary 
	: public IUnknownLibrary
	, public IEngineEventListener
{
public:
	InputLibrary();
	~InputLibrary();

public:
	// IUnknownLibrary
	virtual void            Initialize() override;
	virtual void            OnUpdate() override;
	virtual const char*     GetLibraryName() override { return "Input"; };
	virtual void			Release() override;
	virtual void			RegisterEnv(SInitializeParams& params) override;
	//~IUnknownLibrary

public:
	// IEngineEventListener
	virtual void OnEngineEvent(const EEngineEvents& event) override;
	//~IEngineEventListener
	
private:
	CInput* m_pInput;
	SInitializeParams m_engineParams;
};

REGISTER_UNKNOWN_LIB(InputLibrary);

#endif