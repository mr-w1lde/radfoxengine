/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 13/02/2019  22:34 : Created by Shcherbakov Michail
*************************************************************************/
#include "InputLibrary.h"
#include "Input/Input.h"

#include <Render/IRenderer.h>

InputLibrary::InputLibrary()
{
}

InputLibrary::~InputLibrary()
{
	gEngine->pCore->GetIEngineEvent()->RemoveListener(this);
	SAFE_DELETE(m_pInput);
}

void InputLibrary::Initialize()
{
	m_pInput = new CInput();
	gEngine->pInput = m_pInput;

	gEngine->pCore->GetIEngineEvent()->AddListener(this);
}

void InputLibrary::OnUpdate()
{
	if (m_pInput)
	{
		m_pInput->Update();
		static_cast<GLFWwindow*>(gEngine->pRenderer->GetRenderWindow());
	}
}

void InputLibrary::Release()
{
	gEngine->pLog->Log("[%s] Releasing...", GetLibraryName());
	this->~InputLibrary();
}

void InputLibrary::RegisterEnv(SInitializeParams & params)
{
	gEngine = params.pCore->GetGlobalVariable();
	gEngine->pLog->LogSuccess("[%s] Library has been loaded.", GetLibraryName());
	Initialize();

	m_engineParams = params;
}

void InputLibrary::OnEngineEvent(const EEngineEvents & event)
{
	switch (event)
	{
		case EVENT_RENDER_WINDOW_INIT_COMPLETE:
		{
			TODO("Register GLFW Inpit use pMainRenderWindow pointer");
			break;
		}
		case EVENT_ENGINE_LIBS_LOADED:
		{
			m_pInput->Initialize();
			TODO("Register Input CVars");
			break;
		}
	}
}
