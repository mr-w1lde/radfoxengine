/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 20/02/2019  22:10 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _GLFW_STDADX_H__
#define _GLFW_STDADX_H__

#define ENGINE_API_EXPORT

#include <Engine/enginecore.h>
#include <Core/ILogSystem.h>

#define MODULE_TITILE "[GLFW] "

#define glfwLog(...) do {  gEngine->pLog->Log(MODULE_TITILE __VA_ARGS__); } while(0)
#define glfwLogWarngin(...) do {  gEngine->pLog->LogWarning(MODULE_TITILE __VA_ARGS__); } while(0)
#define glfwLogError(...) do {  gEngine->pLog->LogError(MODULE_TITILE __VA_ARGS__); } while(0)
#define glfwLogSuccess(...) do {  gEngine->pLog->LogSuccess(MODULE_TITILE __VA_ARGS__); } while(0)

#define FATAL_ERROR(x) { RadMessageBox("FATAL ERROR", x, eBT_Error); }

#endif
