/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 13/02/2019  23:10 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _GLFW_LABRARY_H_
#define _GLFW_LABRARY_H_

#include <UnkownLibrary/IUnknownLibrary.h>

class CEngineGlfw
	: public IUnknownLibrary
	, public IEngineEventListener
{
public:
	CEngineGlfw();
	~CEngineGlfw();

public:
	// IUnknownLibrary
	virtual void            Initialize() override;
	virtual void            OnUpdate() override {}
	virtual const char*     GetLibraryName() override { return "GLFW"; };
	virtual void			Release() override;
	virtual void			RegisterEnv(SInitializeParams& params) override;
	//~IUnknownLibrary
public:
	// IEngineEventListener
	virtual void OnEngineEvent(const EEngineEvents& event) override;
	//~IEngineEventListener
	
private:
	SInitializeParams m_engineParams;
};

REGISTER_UNKNOWN_LIB(CEngineGlfw);

#endif