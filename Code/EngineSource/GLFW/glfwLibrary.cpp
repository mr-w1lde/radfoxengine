/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 13/02/2019  23:10 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "glfwLibrary.h"

#include <glfw3.h>

CEngineGlfw::CEngineGlfw()
{
}

CEngineGlfw::~CEngineGlfw()
{
	glfwTerminate();
}

void CEngineGlfw::Initialize()
{
	if (glfwInit() == GLFW_FALSE)
	{
		FATAL_ERROR("GLFW wasn't initialized!");
	}

	gEngine->pCore->GetIEngineEvent()->EngineEvent(EVENT_GLFW_INIT_COMPLETE);
}

void CEngineGlfw::Release()
{
	this->~CEngineGlfw();
}

void CEngineGlfw::RegisterEnv(SInitializeParams & params)
{
	gEngine = params.pCore->GetGlobalVariable();
	gEngine->pLog->LogSuccess("[%s] Library has been loaded.", GetLibraryName());
}

void CEngineGlfw::OnEngineEvent(const EEngineEvents & event)
{
	switch (event)
	{
		case EVENT_ENGINE_LIBS_LOADED:
		{
			Initialize();
			break;
		}
	}
}
