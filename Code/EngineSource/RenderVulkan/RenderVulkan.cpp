/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 01/02/2019  17:45 : Created	by Stanislav Migunov
- 01/02/2019  20:07 : Last edit by Denis Beloedoff
*************************************************************************/
#include "StdAfx.h"
#include "RenderVulkan.h"

#include <Render/IRenderer.h>

CRenderVulkan::CRenderVulkan()
{
	m_pRenderStartup = new CVulkanStartup();
}

CRenderVulkan::~CRenderVulkan()
{
	gEngine->pCore->GetIEngineEvent()->RemoveListener(this);
	m_pRenderStartup->Release();
}

void CRenderVulkan::Initialize()
{
	gEngine->pCore->GetIEngineEvent()->AddListener(this);

	RenderLog("Initializing Vulkan Render...");
	m_pRenderStartup->Initialize();
}

void CRenderVulkan::Release()
{
	RenderLog("Releasing Vulkan Render...");
	this->~CRenderVulkan();
}

void CRenderVulkan::RegisterEnv(SInitializeParams & params)
{
	gEngine = params.pCore->GetGlobalVariable();
	gEngine->pLog->LogSuccess("[%s] Library has been loaded.", GetLibraryName());
}

void CRenderVulkan::OnEngineEvent(const EEngineEvents & event)
{
	
}
