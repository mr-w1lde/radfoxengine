/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 01/02/2019  17:18 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _RENDER_NULL_STDADX_H__
#define _RENDER_NULL_STDADX_H__

#define ENGINE_API_EXPORT
#define VK_DEBUG_LAYERS_LOGGING // For Debug Vulkan

#include <Engine/enginecore.h>
#include <Core/ILogSystem.h>


#define MODULE_TITILE "[RenderVulkan] "

#define RenderLog(...) do {  gEngine->pLog->Log(MODULE_TITILE __VA_ARGS__); } while(0)
#define RenderLogWarngin(...) do {  gEngine->pLog->LogWarning(MODULE_TITILE __VA_ARGS__); } while(0)
#define RenderLogError(...) do {  gEngine->pLog->LogError(MODULE_TITILE __VA_ARGS__); } while(0)
#define RenderLogSuccess(...) do {  gEngine->pLog->LogSuccess(MODULE_TITILE __VA_ARGS__); } while(0)

#define FATAL_ERROR(x) { RadMessageBox("FATAL ERROR", x, eBT_Error); }

#endif
