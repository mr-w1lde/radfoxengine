/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 26/12/2018  21:49 : Created	by Denis Beloedoff
- 02/02/2019  05:21 : Last edit by Stanislav Migunov
*************************************************************************/
#include <stdexcept>
#include <cstdlib>
#include <optional>

#include <VulkanSDK/Include/vulkan/vulkan.h>
#include <glfw/include/glfw3.h>

#include <Render/Uknown/IUknownStartup.h>

#ifndef _VULKAN_STARTUP_H__
#define _VULKAN_STARTUP_H__

struct QueueFamilyIndices 
{
public:
	std::optional<uint32_t> m_graphicsFamily;

public:
	bool IsComplete() 
	{
		return m_graphicsFamily.has_value();
	}
};

class CVulkanStartup : public IUnkownStartup
{

public:
	CVulkanStartup() {}
	virtual ~CVulkanStartup();

public:
	// IUnknownStartup
	virtual bool							Initialize() override;
	virtual void							Release() override;
	virtual void							SetupRenderWindow(void* windowPtr) override;
	//~IUknwonStartup

protected:
	bool									 CreateVKInstance();
	bool									 CreateVKPhysicalDevice();
	bool                                     CheckVKValidationLayerSupport();
	void                                     SetupDebugMessenger();
	bool                                     IsDeviceSuitable(VkPhysicalDevice device);
	QueueFamilyIndices                       FindQueueFamilies(VkPhysicalDevice device);
	std::vector<const char*>                 GetRequiredExtensions();
	static VKAPI_ATTR VkBool32 VKAPI_CALL    DebugCallback
	(
		VkDebugUtilsMessageSeverityFlagBitsEXT			messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT					messageType,
		const VkDebugUtilsMessengerCallbackDataEXT*		pCallbackData,
		void*											pUserData
	);

private:
	GLFWwindow*							m_pWindow = nullptr;
	VkInstance							m_vkInstance = NULL;
	VkDebugUtilsMessengerEXT			m_debugMessenger = NULL;
	const std::vector<const char*>		m_validationLayers = {};
	std::vector<VkPhysicalDevice>		m_vkPhysicalDevices = {};
	VkPhysicalDevice					m_physicalDevice = VK_NULL_HANDLE;
};
#endif
