/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 01/02/2019  17:18 : Created	by Stanislav Migunov
- 01/02/2019  20:07 : Last edit by Denis Beloedoff
*************************************************************************/
#include <Render/Null/IRenderUknown.h>
#include "Startup/VulkanStartup.h"

#include <UnkownLibrary/IUnknownLibrary.h>

#ifndef _RENDER_VULKAN_H__
#define _RENDER_VULKAN_H__

class CRenderVulkan 
	: public IRenderUknown
	, public IUnknownLibrary
	, public IEngineEventListener
{
public:
	CRenderVulkan();
	virtual ~CRenderVulkan();

public:
	//IRenderUknown
	virtual ERenderUknown	GetUknownRender() override { return ERenderUknown::eRU_Vulkan; }
	//~IRenderUknown
public:
	// IUnknownLibrary
	virtual void            Initialize() override;
	virtual void			Release() override;
	virtual const char*     GetLibraryName() override { return "RenderVulkan"; };
	virtual void			RegisterEnv(SInitializeParams& params) override;
	//~IUnknownLibrary
public:
	// IEngineEventListener
	virtual void OnEngineEvent(const EEngineEvents& event) override;
	//~IEngineEventListener
private:
	CVulkanStartup* m_pRenderStartup;
};

REGISTER_UNKNOWN_LIB(CRenderVulkan)

#endif // !_RENDER_VULKAN_H__
