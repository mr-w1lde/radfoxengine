/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 18/01/2019  17:29 : Created by Stanislav Migunov
*************************************************************************/
#include <Render/Null/IRenderNull.h>
#include <Render/Null/IRenderUknown.h>

#include <UnkownLibrary/IUnknownLibrary.h>

#include <RFLibrary.h>

#ifndef _RENDER_NULL_H__
#define _RENDER_NULL_H__

class CRenderWindow;
class CRenderer;

class CRenderNullLibrary 
	: public IUnknownLibrary
	, public IEngineEventListener
{
public:
	CRenderNullLibrary();
	virtual ~CRenderNullLibrary();

public:
	// IUnknownLibrary
	virtual void            Initialize() override;
	virtual void			Release() override;
	virtual void            OnUpdate() override;
	virtual const char*     GetLibraryName() override { return "RenderNull"; };
	virtual void			RegisterEnv(SInitializeParams& params) override;
	//~IUnknownLibrary
public:
	// IEngineEventListener
	virtual void OnEngineEvent(const EEngineEvents& event) override;
	//~IEngineEventListener
public:
	void RegisterCVars();
	void RenderInit();
private:
	IUnknownLibrary* m_pUknownRenderLibrary;
	SInitializeParams  m_engineParams;
private:
	CRenderWindow* m_pRenderWindow;
	CRenderer* m_pRenderer;
};

REGISTER_UNKNOWN_LIB(CRenderNullLibrary);

#endif 
