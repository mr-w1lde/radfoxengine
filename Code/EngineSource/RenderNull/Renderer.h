/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 19/02/2019  19:06 : Created by Stanislav Migunov
*************************************************************************/

#ifndef _RENDERER_WINDOW_H__
#define _RENDERER_WINDOW_H__

#include <Render/IRenderer.h>

class CRenderer : public IRenderer
{
public:
	CRenderer();
	~CRenderer();

public:
	// IRenderer
	virtual void* GetRenderWindow() override { return m_pMainWindowRender; };
	//~IRenderer

public:
	void SetRenderWindow(void* ptr) { m_pMainWindowRender = ptr; }

private:
	void* m_pMainWindowRender;
};


#endif 
