/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 18/01/2019  17:29 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "RenderNull.h"
#include "RenderWindow/RenderWindow.h"
#include "Renderer.h"

#include <Core/Library/IEngineLibrary.h>
#include <Core/Console/IConsole.h>
#include <Core/Parsers/IEngineIniParser.h>

CRenderNullLibrary::CRenderNullLibrary()
	: m_pUknownRenderLibrary(nullptr)
	, m_engineParams(SInitializeParams())
{
}

CRenderNullLibrary::~CRenderNullLibrary()
{
	gEngine->pCore->GetIEngineEvent()->RemoveListener(this);
}

void CRenderNullLibrary::Initialize()
{
	gEngine->pCore->GetIEngineEvent()->AddListener(this);
	m_pRenderWindow = new CRenderWindow();

	m_pRenderer = new CRenderer();
	gEngine->pRenderer = m_pRenderer;
}


void CRenderNullLibrary::Release()
{
	RenderLog("Releasing RenderNull Library...");
	this->~CRenderNullLibrary();
}

void CRenderNullLibrary::OnUpdate()
{
	if (m_pRenderWindow)
	{
		m_pRenderWindow->Update();
	}
}

void CRenderNullLibrary::RegisterEnv(SInitializeParams & params)
{
	gEngine = params.pCore->GetGlobalVariable();
	gEngine->pLog->LogSuccess("[%s] Library has been loaded.", GetLibraryName());
	
	m_engineParams = params;

	Initialize();
}

void CRenderNullLibrary::OnEngineEvent(const EEngineEvents & event)
{
	switch (event)
	{
		case EVENT_ENGINE_LIBS_LOADED:
		{
			RegisterCVars();
			RenderInit();

			break;
		}

		case EVENT_RENDER_WINDOW_INIT_COMPLETE:
		{
			m_pUknownRenderLibrary->Initialize();
			break;
		}
	}
}

void CRenderNullLibrary::RegisterCVars()
{
	const char* sSection = "Render";
	gEngine->pConsole->RegisterSection(sSection);

	REGISTER_CVAR_STRING(SCVarId(sSection, "Api"), "Vulkan", "Type of loading api...");
	REGISTER_CVAR_BOOLEAN(SCVarId(sSection, "Fullscreen"), false, "Fullscreen variable");
	REGISTER_CVAR_INT(SCVarId(sSection, "Width"), 1024, "null");
	REGISTER_CVAR_INT(SCVarId(sSection, "Height"), 768, "null");
	REGISTER_CVAR_BOOLEAN(SCVarId(sSection, "IsResize"), false, "null");
}

void CRenderNullLibrary::RenderInit()
{
	IEngineIniParser* pIniParser = gEngine->pCore->GetIEngineIniParser();

	IInih* pRenderIni = pIniParser->LoadFile("Engine/config.ini");

	for (auto& it : pRenderIni->GetFields("Render"))
	{
		ICVar* pCurr = gEngine->pConsole->GetCVar(SCVarId("Render", it.c_str()));

		if (!pCurr)
			return;

		if (pCurr->GetType() == TID_BOOL)
		{
			pCurr->SetVal((bool)pRenderIni->GetBoolean("Render", it, false));
		}
		else if (pCurr->GetType() == TID_INT)
		{
			pCurr->SetVal((int)pRenderIni->GetInteger("Render", it, 0));
		}
		else if (pCurr->GetType() == TID_FLOAT)
		{
			pCurr->SetVal((float)pRenderIni->GetReal("Render", it, 0.f));
		}
		else if (pCurr->GetType() == TID_STRING)
		{
			pCurr->SetVal((string)pRenderIni->GetString("Render", it, " "));
		}
	}

	ICVar* pRenderApi = gEngine->pConsole->GetCVar(SCVarId("Render", "Api"));

	if (pRenderApi->GetValString() == string("Vulkan"))
	{
		m_pUknownRenderLibrary = (IUnknownLibrary*)gEngine->pCore->GetIIEngineLibrary()->LoadUnknownLibrary("Engine.RenderVulkan", m_engineParams);
	}
	else if (pRenderApi->GetValString() == string("DX12"))
	{
		FATAL_ERROR("DX12 Unsupported now.");
	}
	else
	{
		FATAL_ERROR("Unknown Render Type.");
	}

	m_pRenderWindow->Initialize();
	m_pRenderer->SetRenderWindow(m_pRenderWindow->CreateRenderWindow());


	gEngine->pCore->GetIEngineEvent()->EngineEvent(EVENT_RENDER_WINDOW_INIT_COMPLETE);
}
