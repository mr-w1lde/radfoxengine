/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 15/02/2019  02:06 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "RenderWindow.h"

#include <Core/Console/IConsole.h>

CRenderWindow::CRenderWindow()
	: m_pRenderWindow(nullptr)
{
}

CRenderWindow::~CRenderWindow()
{

}

void CRenderWindow::Initialize()
{

	ICVar* pFullScreenVar = gEngine->pConsole->GetCVar(SCVarId("Render", "Fullscreen"));

	if (pFullScreenVar)
	{
		m_bFullscreen = pFullScreenVar->GetValBool();
	}

	ICVar* pRenderWidth = gEngine->pConsole->GetCVar(SCVarId("Render", "Width"));
	ICVar* pRenderHeight = gEngine->pConsole->GetCVar(SCVarId("Render", "Height"));

	if (pRenderWidth && pRenderHeight)
	{
		m_iWidth = pRenderWidth->GetValInt();
		m_iHeight = pRenderHeight->GetValInt();
	}

	if (m_iWidth < 800 || m_iHeight < 640)
		FATAL_ERROR("Size of render window too small!");
}

void CRenderWindow::Release()
{
}

void CRenderWindow::Update()
{
	/*if (m_pRenderWindow)
	{
		if (!glfwWindowShouldClose(m_pRenderWindow))
		{
			glfwPollEvents();
		}
		else
		{
			gEngine->pCore->Quit();
		}
	}*/
}

GLFWwindow* CRenderWindow::CreateRenderWindow()
{
	/*GLFWmonitor *pMainWindow = nullptr;

	if (m_bFullscreen)
		pMainWindow = glfwGetPrimaryMonitor();

	if (m_sTitle.empty())
	{
		m_sTitle =	ENGINE_NAME " " +
					std::to_string(ENGINE_VERSION_MAJOR) + "." +
					std::to_string(ENGINE_VERSION_MINOR) + "." +
					std::to_string(ENGINE_VERSION_BUILD);
	}

	m_pRenderWindow = glfwCreateWindow(m_iWidth, m_iHeight, m_sTitle.c_str(), pMainWindow, nullptr);

	if (m_pRenderWindow == nullptr)
	{
		FATAL_ERROR("Couldn't create GLFW Window...");
	}

	return m_pRenderWindow;*/

	return nullptr;
}
