/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 15/02/2019  02:06 : Created by Stanislav Migunov
*************************************************************************/

#ifndef _RENDER_WINDOW_H__
#define _RENDER_WINDOW_H__

struct GLFWwindow;

class CRenderWindow
{
public:
	CRenderWindow();
	~CRenderWindow();

public:
	void Initialize();
	void Release();
	void Update();

	GLFWwindow* CreateRenderWindow();

private:
	GLFWwindow* m_pRenderWindow;
	int m_iWidth;
	int m_iHeight;
	string m_sTitle;
	bool m_bFullscreen;
};


#endif 
