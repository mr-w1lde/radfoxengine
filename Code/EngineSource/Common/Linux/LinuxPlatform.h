/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 20/01/2019  14:10 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _RAD_FOX_COMMON_LINUX_PLATFORM_SPECIFIC_
#define _RAD_FOX_COMMON_LINUX_PLATFORM_SPECIFIC_

// Define platform independent types.
#include "EngineBaseTypes.h"

#define DLL_EXPORT __attribute__ ((dllexport))
#define DLL_IMPORT __attribute__ ((dllimport))

#define ILINE __always_inline

#define PRAGMA(x) _Pragma(#x)

typedef long LONG;
typedef unsigned char BYTE;

#ifndef SAFE_DELETE
#define SAFE_DELETE(p) { if(p) { delete (p); (p)=NULL; } }
#endif

#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) { if(p) { delete [] (p); (p)=NULL; } }
#endif

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) { if(p) { (p)->Release(); (p)=NULL; } }
#endif

#endif