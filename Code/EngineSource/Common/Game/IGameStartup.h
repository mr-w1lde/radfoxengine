/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 22/12/2018  20:39 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _IGAME_STARTUP_H__
#define _IGAME_STARTUP_H__

struct SInitializeParams;

struct IGameStartup
{
	//Entry point of IGameStartup
	typedef IGameStartup *(*TEntryPoint)();
	
	virtual ~IGameStartup() {}
	
	//Init function
	virtual bool Initialize(SInitializeParams& params) = 0;
	
	//Shutdown function
	virtual void Shutdown() = 0;
	
	//Run function
	virtual int Run() = 0;
};

#endif