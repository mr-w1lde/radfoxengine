/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 22/12/2018  20:39 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _RAD_FOX_COMMON_WIN64_PLATFORM_SPECIFIC_
#define _RAD_FOX_COMMON_WIN64_PLATFORM_SPECIFIC_

// Ensure WINAPI version is consistent everywhere

#define _CPU_AMD64
#define _CPU_SSE
#define ILINE __forceinline

#define DEBUG_BREAK do { __debugbreak(); } while(0)
#define DEPRICATED __declspec(deprecated)
#define TYPENAME(x) typeid(x).name()
#define SIZEOF_PTR 8

#pragma warning( disable : 4267 ) //warning C4267: 'initializing' : conversion from 'size_t' to 'unsigned int', possible loss of data

// Standard includes.
#include <malloc.h>
#include <io.h>
#include <new.h>
#include <direct.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <dxgi.h>
#include <Windows.h>
//////////////////////
// Special intrinsics
#include <math.h> // Should be included before intrin.h
#include <intrin.h>

// Define platform independent types.
#include "EngineBaseTypes.h"

#define DLL_EXPORT __declspec(dllexport)
#define DLL_IMPORT __declspec(dllimport)

#define PRAGMA(x) __pragma(x)

#define THREADID_NULL -1
typedef long LONG;
typedef unsigned char BYTE;
typedef unsigned long threadID;
typedef unsigned long DWORD;
typedef double        real; 
typedef long          LONG;

typedef void *THREAD_HANDLE;
typedef void *EVENT_HANDLE;

typedef LONG NTSTATUS, *PNTSTATUS;
typedef NTSTATUS(WINAPI* RtlGetVersionPtr)(PRTL_OSVERSIONINFOW);

typedef __int64 INT_PTR, *PINT_PTR;
typedef unsigned __int64 UINT_PTR, *PUINT_PTR;

typedef __int64 LONG_PTR, *PLONG_PTR;
typedef unsigned __int64 ULONG_PTR, *PULONG_PTR;

typedef ULONG_PTR DWORD_PTR, *PDWORD_PTR;


#ifndef SAFE_DELETE
#define SAFE_DELETE(p) { if(p) { delete (p); (p)=NULL; } }
#endif

#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) { if(p) { delete [] (p); (p)=NULL; } }
#endif

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) { if(p) { (p)->Release(); (p)=NULL; } }
#endif

#endif
