/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 01/02/2019  17:02 : Created by Stanislav Migunov
*************************************************************************/

#ifndef _IRENDER_NULL_H__
#define _IRENDER_NULL_H__

struct SInitializeParams;

struct IRenderNull
{
	typedef IRenderNull *(*TEntryPoint)();

	virtual void Initialze(SInitializeParams& params) = 0;
	virtual void Update() = 0;
	virtual void Release() = 0;

	virtual ~IRenderNull() {}
};

#endif