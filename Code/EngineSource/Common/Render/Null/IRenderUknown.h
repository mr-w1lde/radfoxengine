/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 01/02/2019  17:02 : Created by Stanislav Migunov
*************************************************************************/

#ifndef _IRENDER_UKNOWN_H__
#define _IRENDER_UKNOWN_H__

struct SInitializeParams;

enum class ERenderUknown : int
{
	eRU_Vulkan = 0,
	eRU_OpenGL,
	eRU_DX12
};

struct IRenderUknown
{
	typedef IRenderUknown *(*TEntryPoint)();

public:
	virtual ERenderUknown GetUknownRender() = 0;

	~IRenderUknown() {}
};

#endif