/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 02/02/2019  4:57 : Created by Stanislav Migunov
*************************************************************************/

#ifndef _IUKNWON_STARTUP_H__
#define _IUKNWON_STARTUP_H__

struct IUnkownStartup
{
	virtual bool Initialize() = 0;
	virtual void Release() = 0;
	virtual void SetupRenderWindow(void* windowPtr) = 0;

	virtual ~IUnkownStartup() {}

};

#endif