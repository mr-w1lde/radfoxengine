/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 019/02/2019  19:03 : Created by Stanislav Migunov
*************************************************************************/

#ifndef _IENGINE_RENDER_H__
#define _IENGINE_RENDER_H__


struct IRenderer
{
	virtual ~IRenderer() {}

public:
	virtual void* GetRenderWindow() = 0;
};

#endif