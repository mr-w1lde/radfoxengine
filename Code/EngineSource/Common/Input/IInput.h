/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 12/02/2019  09:44 : Created by Stanislav Migunov
*************************************************************************/
#include <map>
#include <string>

#ifndef _IINPUT_H__
#define _IINPUT_H__

enum EInputState
{
	eIS_OnReleased = 0,
	eIS_OnPressed,
	eIS_OnHold
};

struct IInputActionListener
{
	virtual void OnAction(int key, int state, float value) = 0;
};

template<class C>
struct IActionHandler
{
public:
	IActionHandler()
	{
		IInih* pReader = gEngine->pCore->GetIEngineIniParser()->LoadFile("engine/InputKeysButton.ini");

		if (pReader->GetParsError() < 0)
		{
			InputLogError("Can't load InputKeysButton.ini file.");
		}

		std::set<std::string> sections = pReader->GetSections();

		for (std::set<std::string>::iterator sectionsIt = sections.begin(); sectionsIt != sections.end(); sectionsIt++)
		{
			std::set<std::string> fields = pReader->GetFields(*sectionsIt);
			for (std::set<std::string>::iterator fieldsIt = fields.begin(); fieldsIt != fields.end(); fieldsIt++)
			{
				int key = pReader->GetInteger(*sectionsIt, *fieldsIt, -1);
				m_buttonKeys.emplace(*fieldsIt, key);
			}
		}
	}
	~IActionHandler() {}

private:
	typedef bool(C::*HandlerFuncPtr)(int, float);
	typedef std::map<int, HandlerFuncPtr> InputFunc;
	typedef std::map<std::string, int> ButtonKeys;

	typedef std::map<std::string, int> FieldKeys;
	typedef std::map<std::string, FieldKeys> Sections;
	typedef std::map<std::string, bool> AvailableSections;

public:
	void AddHandler(const char* section, const char* field, HandlerFuncPtr func)
	{
		auto isAvaible = m_availableSections.find(section);

		if (isAvaible->second)
		{
			auto Section = m_sections.find(section)->second;
			int key = Section.find(field)->second;
			m_ptrFuncs.emplace(key, func);
		}
	}
	void Action(int key, int state, float value)
	{
		if (m_ptrFuncs.find(key) != m_ptrFuncs.end())
		{
			HandlerFuncPtr itFunc = m_ptrFuncs.at(key);
			(static_cast<C*>(m_pInstance)->*itFunc)(state, value);
		}
	}

public:
	void LoadFile(const char* LocalPath)
	{
		IInih* pReader = gEngine->pCore->GetIEngineIniParser()->LoadFile(LocalPath);

		if (pReader->GetParsError() < 0)
		{
			InputLogError("Can't load GameInputKeys.ini file.");
		}
		std::set<std::string> sections = pReader->GetSections();

		for (std::set<std::string>::iterator sectionsIt = sections.begin(); sectionsIt != sections.end(); sectionsIt++)
		{
			FieldKeys fieldKeys;

			std::set<std::string> fields = pReader->GetFields(*sectionsIt);
			for (std::set<std::string>::iterator fieldsIt = fields.begin(); fieldsIt != fields.end(); fieldsIt++)
			{
				int key;
				std::string str = pReader->GetString(*sectionsIt, *fieldsIt, "");
				if (str.length() == 1)
				{
					key = str[0];
				}
				else
				{
					key = m_buttonKeys.find(str)->second;
				}
				fieldKeys.emplace(*fieldsIt, key);
			}
			m_sections.emplace(*sectionsIt, fieldKeys);
			m_availableSections.emplace(*sectionsIt, true);
		}
	}
	void AvailableSection(std::string section, bool flag)
	{
		if (m_availableSections.find(section) != m_availableSections.end())
		{
			m_availableSections.find(section)->second = flag;
		}
	}

private:
	C* m_pInstance;
	InputFunc m_ptrFuncs;
	ButtonKeys m_buttonKeys;
	Sections m_sections;
	AvailableSections m_availableSections;
};

struct IInput
{
	virtual void Initialize() = 0;
	virtual void Release() = 0;
	virtual void Update() = 0;

	virtual void AddListener(IInputActionListener* listener) = 0;
	virtual void RemoveListener(IInputActionListener* listener) = 0;
};

#endif