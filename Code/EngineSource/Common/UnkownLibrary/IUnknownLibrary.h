/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 02/02/2019  10:10 : Created by Stanislav Migunov
*************************************************************************/
#include "Engine/enginecore.h"
#include <RFLibrary.h>

#define REGISTER_UNKNOWN_LIB(cname) \
extern "C"							\
{									\
	RADCORE_API IUnknownLibrary* CreateUknownLibraryStartup() \
	{								\
		return new cname();         \
	}								\
};                                  \

#ifndef _IUNKNOWN_LIBRARY_H__
#define _IUNKNOWN_LIBRARY_H__

struct SInitializeParams;

struct IUnknownLibrary
{
	typedef IUnknownLibrary* (*TEntryPoint)();

public:
	virtual void            Initialize() = 0;
	virtual void            OnUpdate() {};
	virtual void			Release() = 0;
	virtual const char*     GetLibraryName() = 0;

	virtual void			RegisterEnv(SInitializeParams& params)
	{
		gEngine = params.pCore->GetGlobalVariable();
		gEngine->pLog->LogSuccess("[%s] Library has been loaded.", GetLibraryName());
		Initialize();
	}

	virtual ~IUnknownLibrary() {}
};

#endif


