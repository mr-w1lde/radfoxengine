/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 22/12/2018  20:39 : Created by Stanislav Migunov
*************************************************************************/
#ifndef __RFLIBRARY_H__
#define __RFLIBRARY_H__

#define Library_Game			"Engine.GameDll"
#define Library_Core			"Engine.Core"
#define Library_RenderNull		"Engine.RenderNull"
#define Library_RenderVulkan	"Engine.RenderVulkan"

#include "Engine/enginecore.h"

#if defined(USE_PLATFORM_WINDOWS)
	typedef HMODULE RadDynamic;
#endif

#if defined(USE_PLATFORM_LINUX)
	#include <dlfcn.h>
	#include <errno.h>

	typedef void* RadDynamic;
#endif

static RadDynamic RadLoadLibrary(string path)
{
	RadDynamic rdDll = nullptr;
	string sExeption = "";

#if defined (USE_PLATFORM_WINDOWS)
	string dllPath = path + ".dll";
	rdDll = LoadLibraryA(dllPath.c_str());
#endif

#if defined(USE_PLATFORM_LINUX)
	string soPath = "./lib" + path + ".so";
	rdDll = dlopen(soPath.c_str(), RTLD_NOW | RTLD_LOCAL);
#endif 

	return rdDll;
}

template <class D>
static D RadProcAddress(RadDynamic radDynamic, const char* func)
{
#if defined (USE_PLATFORM_WINDOWS)
	return reinterpret_cast<D>(GetProcAddress(radDynamic, func));
#endif

#if defined(USE_PLATFORM_LINUX)
	return reinterpret_cast<D>(dlsym(radDynamic, func));
#endif 
}

static void RadLibraryFree(RadDynamic radDynamic)
{
#if defined (USE_PLATFORM_WINDOWS)
	FreeLibrary(radDynamic);
#endif

#if defined(USE_PLATFORM_LINUX)
	dlclose(radDynamic);
#endif 

}

#endif
