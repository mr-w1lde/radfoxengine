/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 18/01/2019  18:30 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _ILOGSYSTEM_H__
#define _ILOGSYSTEM_H__

//! Compiler-supported type-checking helper
#define PRINTF_PARAMS(...)
#define SCANF_PARAMS(...)

enum ELogType
{
	eLT_Log = 0,
	eLT_LogError,
	eLT_LogWarning,
	eLT_LogSuccess,
	eLT_FatalError
};

struct ILogEventCallback
{
	virtual void OnLogWrite(const char* msg, ELogType type) = 0;
};

struct ILogSystem
{
	virtual void Log(const char* msg, ...) PRINTF_PARAMS(2, 3) = 0;
	virtual void LogError(const char* msg, ...) PRINTF_PARAMS(2, 3) = 0;
	virtual void LogWarning(const char* msg, ...) PRINTF_PARAMS(2, 3) = 0;
	virtual void LogSuccess(const char* msg, ...) PRINTF_PARAMS(2, 3) = 0;
	virtual void FatalError(const char* msg) = 0;

	virtual void AddListener(ILogEventCallback* listener) = 0;
	virtual void RemoveListener(ILogEventCallback* listener) = 0;

};

#endif
