/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 02/02/2019  10:07 : Created by Stanislav Migunov
*************************************************************************/

#include <RFLibrary.h>

#ifndef _IENGINE_LIBRARY_H__
#define _IENGINE_LIBRARY_H__

struct SInitializeParams;

struct IEngineLibrary
{
	virtual void 	Initialize() = 0;
	virtual void 	Release() = 0;
	virtual void* 	LoadUnknownLibrary(const char* lib, SInitializeParams& params) = 0;
};

#endif