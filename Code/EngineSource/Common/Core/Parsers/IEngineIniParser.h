/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 03/02/2019  17:23 : Created by Stanislav Migunov
*************************************************************************/

#include <string>
#include <set>

#ifndef _IENGINE_INIPARSER_H__
#define _IENGINE_INIPARSER_H__

class INIReader;

struct IInih
{
	// Return the result of ini_parse(), i.e., 0 on success, line number of
	// first error on parse error, or -1 on file open error.
	virtual int GetParsError() = 0;

	// Get a string value from INI file, returning default_value if not found.
	virtual std::string GetString(std::string section, 
								  std::string name, 
								  std::string default_value) = 0;
	// Get an integer (long) value from INI file, returning default_value if
	// not found or not a valid integer (decimal "1234", "-1234", or hex "0x4d2").
	virtual long GetInteger(std::string section, std::string name, long default_value) = 0;
	// Get a real (floating point double) value from INI file, returning
	// default_value if not found or not a valid floating point value
	// according to strtod().
	virtual double GetReal(std::string section, std::string name, double default_value) = 0;
	// Get a boolean value from INI file, returning default_value if not found or if
	// not a valid true/false value. Valid true values are "true", "yes", "on", "1",
	// and valid false values are "false", "no", "off", "0" (not case sensitive).
	virtual bool GetBoolean(std::string section, std::string name, bool default_value) = 0;

	// Returns all the section names from the INI file, in alphabetical order, but in the
	// original casing
	virtual std::set<std::string> GetSections() const = 0;

	// Returns all the field names from a section in the INI file, in alphabetical order,
    // but in the original casing. Returns an empty set if the field name is unknown
	virtual std::set<std::string> GetFields(std::string section) const = 0;
};

struct IEngineIniParser
{
	virtual void Initialize() = 0;
	virtual IInih* LoadFile(const char* path) = 0;
};

#endif