/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 02/02/2019  7:57 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _ICFUNC_H__
#define _ICFUNC_H__

struct IConsoleFuncArgs
{
	virtual ~IConsoleFuncArgs() {}
public:
	virtual int GetArgsCount() = 0;
	virtual const char* GetArg(int index) = 0;
	virtual const char* GetLine() = 0;
};

struct IConsoleFunc
{
	typedef void(*ConsoleFunctionPtr)(IConsoleFuncArgs*);
};

#endif