/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 02/02/2019  7:57 : Created by Stanislav Migunov
*************************************************************************/
#include <boost/variant.hpp>

#include "Engine/enginecore.h"

typedef boost::variant<int, bool, float, std::string> TVarValue;

#define TID_BOOL typeid(bool)
#define TID_INT typeid(int)
#define TID_FLOAT typeid(float)
#define TID_STRING typeid(string)

#define REGISTER_CVAR_INT(id, val, help) { gEngine->pConsole->RegisterVar((SCVarId)id, (int)val, help); }
#define REGISTER_CVAR_FLOAT(id, val, help) { gEngine->pConsole->RegisterVar((SCVarId)id, (float)val, help); }
#define REGISTER_CVAR_STRING(id, val, help) { gEngine->pConsole->RegisterVar((SCVarId)id, (string)val, help); }
#define REGISTER_CVAR_BOOLEAN(id, val, help) { gEngine->pConsole->RegisterVar((SCVarId)id, (bool)val, help); }

#ifndef _ICVAR_H__
#define _ICVAR_H__

struct SCVarId
{
private:
	const char* m_section;
	const char* m_var;

public:

	SCVarId(const char* section, const char* var)
	{
		m_section = section;
		m_var = var;
	}

public:
	const char* GetSection() { return m_section; }
	const char* GetVar() { return m_var; }
};

struct ICVar
{
	virtual ~ICVar() {}
public:
	virtual void Release() = 0;

	virtual const char* GetCurrentSection() = 0;
	virtual const char* GetTitle() = 0;
	virtual const char* GetHelp() = 0;

	virtual const char* GetValString() = 0;
	virtual int GetValInt() = 0;
	virtual bool GetValBool() = 0;
	virtual float GetValFloat() = 0;

	virtual void SetVal(TVarValue value) = 0;

	const virtual std::type_info& GetType() const = 0;
};



#endif