/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 02/02/2019  7:57 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _ICONSOLE_H__
#define _ICONSOLE_H__

#include "ICVar.h"
#include "ICFunc.h"

struct ICVar;
struct SCVarId;

struct IConsole
{
	virtual void Initialize() = 0;
	virtual void Release() = 0;

	virtual bool RegisterSection(const char* id) = 0;
	virtual bool RegisterVar(SCVarId id, TVarValue value, const char* help) = 0;

	virtual void RegisterFunction(const char* title, IConsoleFunc::ConsoleFunctionPtr) = 0;

	virtual void RemoveSection(const char* id) = 0;
	virtual void RemoveVar(SCVarId id) = 0;

	virtual ICVar* GetCVar(SCVarId id) = 0;
};

#endif