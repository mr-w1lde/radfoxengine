/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 18/01/2019  18:30 : Created by Stanislav Migunov
*************************************************************************/
#include "Engine/enginecore.h"

#ifndef _IENGINE_CORE_H__
#define _IENGINE_CORE_H__

struct ICoreGlobalVariable;
struct ILogSystem;
struct IEngineCore;
struct IEngineLibrary;
struct IEngineIniParser;
struct IConsole;
struct IEngineEvent;
struct IInput;
struct IRenderer;

#if defined(USE_PLATFORM_WINDOWS)
	enum class EWindowsVersion : int
	{
		eWV_WinUknowun = 0,
		eWV_Win7,
		eWV_Win80,
		eWV_Win81,
		eWV_Win10
	};	
#endif

struct SInitializeParams
{
	ILogSystem* pLogSystem;

	const char* logFileName;
	const char* systemCmdLine;
	const char* rootDir;
	const char* userPath;
	const char* binDir;

	bool bSandbox;
	bool bPreview;
	bool bTesting;

	IEngineCore* pCore;
	SInitializeParams()
	{
		pLogSystem = NULL;
		pCore = NULL;

		logFileName = "";
		systemCmdLine = "";
		rootDir = "";
		userPath = "";
		binDir = "";

		bSandbox = false;
		bPreview = false;
		bTesting = false;
	}
};

enum EEngineEvents
{
	EVENT_ENGINE_INIT = 0,
	EVENT_ENGINE_LIBS_LOADED,

	EVENT_RENDER_WINDOW_INIT_COMPLETE,

	EVENT_GLFW_INIT_COMPLETE,

	EVENT_INVALID = -1,

};

struct IEngineEventListener
{
	virtual ~IEngineEventListener() {}
public:
	virtual void OnEngineEvent(const EEngineEvents& event) = 0;
};

struct IEngineEvent
{
	virtual ~IEngineEvent() {};
public:
	virtual void AddListener(IEngineEventListener* pEngineEvents) = 0;
	virtual void RemoveListener(IEngineEventListener* pEngineEvents) = 0;
	
	virtual void EngineEvent(EEngineEvents event) = 0;
};

struct ICoreGlobalVariable
{
	ICoreGlobalVariable() {};

    ILogSystem*		pLog;
	IEngineCore*	pCore;
	IConsole*		pConsole;
	IInput*			pInput;
	IRenderer*	    pRenderer;

	ILINE const bool IsClient()
	{
		return m_bClient;
	}

	ILINE const bool IsSandbox()
	{
		return m_bSandbox;
	}

private:
	bool m_bClient;
	bool m_bSandbox;
};

struct IEngineCore
{
	//Entry point of IEngineCore
	typedef IEngineCore *(*TEntryPoint)();
	virtual ~IEngineCore() {}

	virtual void						Initialize(SInitializeParams& params) = 0;
	virtual bool						CompleteInit() = 0;
	virtual void						Release() = 0;
	virtual ICoreGlobalVariable*		GetGlobalVariable() = 0;
	virtual const char*					GetRootDir() = 0;
	virtual void                        RunCoreLoop() = 0;
	virtual void						Quit() = 0;
	virtual void                        IsQuiting() = 0;
	virtual ILogSystem*					GetILogSystem() = 0;
	virtual IEngineLibrary*             GetIIEngineLibrary() = 0;
	virtual IEngineIniParser*			GetIEngineIniParser() = 0;
	virtual IEngineEvent*				GetIEngineEvent() = 0;
};


#endif