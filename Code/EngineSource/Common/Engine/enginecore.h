/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 26/12/2018  01:32 : Created by Stanislav Migunov
- 20/01/2019  21:242 : Last edit by Stanislav Migunov
*************************************************************************/

#ifndef _ENGINE_CORE_H__
#define _ENGINE_CORE_H__

#include "RFInfo.h"

#define USE_STD_STRING
#define USE_STD_VECTOR

#if defined(_WIN32)
#define USE_PLATFORM_WINDOWS
#if defined(USE_PLATFORM_WINDOWS)
    #include "Windows/Win64platform.h"
#endif
#endif

#if defined(__GNUC__)
#define USE_PLATFORM_LINUX
#if defined(USE_PLATFORM_LINUX)
    #include <algorithm>
    #include <stdexcept>
    #include "Linux/LinuxPlatform.h"
#endif
#endif

#if defined(USE_STD_VECTOR)
#include <vector>
#endif

#if defined(USE_STD_STRING)
#include <string>
typedef std::string string;
#endif

#if defined(USE_PLATFORM_WINDOWS)
	#define TODO(msg) PRAGMA(message("[TODO]: " __FUNCTION__ "() -> " msg))
#else
    #define TODO(msg) PRAGMA(message("[TODO]: -> " msg))
#endif

#ifdef ENGINE_API_EXPORT
    #define RADCORE_API DLL_EXPORT
#else
    #define RADCORE_API DLL_IMPORT
#endif

#include "Core/IEngineCore.h"
extern ICoreGlobalVariable* gEngine;

#if defined(USE_PLATFORM_WINDOWS)
#include <iostream>
void OpenLogConsole();
void InitRootDir(char szExeFileName[] = 0, unsigned int nSize = 0);
#endif

#ifndef RAD_MESSAGEBOX
#define RAD_MESSAGEBOX

enum EMessageBoxType
{
    eBT_Information = 0,
    eBT_Error,
    eBT_Warning
};

void RadMessageBox(string title, string msg, EMessageBoxType type);

template<class C, class V>
static inline void find_and_erase(C& c, const V& v)
{
	typename C::iterator it = std::find(c.begin(), c.end(), v);
	if (it != c.end())
	{
		c.erase(it);
	}
}

#endif

#ifndef assert
#define assert(x, y, m) { if(x == y) { RadMessageBox("FATAL ERROR", m, eBT_Error); } }
#endif // !assert


#endif