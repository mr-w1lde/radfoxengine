/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 26/12/2018  01:38 : Created by Stanislav Migunov
- 17/01/2019  21:46 : Last edit by Denis Beloedoff 
*************************************************************************/
#include "enginecore.h"
#include <cstdio> 
#include <fstream> 

//Global var
ICoreGlobalVariable* gEngine = nullptr;

#if defined(USE_PLATFORM_WINDOWS)

// For debugging 
#include <io.h> 
#include <fcntl.h> 

void InitRootDir(char* szExeFileName, unsigned int nSize)
{
	WCHAR szExePathName[_MAX_PATH];
	size_t nLen = GetModuleFileNameW(GetModuleHandle(NULL), szExePathName, _MAX_PATH);

	// Find path above exe name and deepest folder.
	int nCount = 0;
	for (size_t n = nLen - 1; n > 0; n--)
	{
		if (szExePathName[n] == '\\')
		{
			nLen = n;
			if (++nCount == 3)
				break;
		}
	}
	if (nCount > 0)
	{
		szExePathName[nLen++] = 0;

		// Switch to upper folder.
		SetCurrentDirectoryW(szExePathName);

		// Return exe name and relative folder, assuming it's ASCII.
		//if (szExeFileName)
			//wcstombs(szExeFileName, szExePathName + nLen, nSize);
	}
}


class outbuf : public std::streambuf {
public:
	outbuf() {
		setp(0, 0);
	}

	virtual int_type overflow(int_type c = traits_type::eof()) {
		return fputc(c, stdout) == EOF ? traits_type::eof() : c;
	}
};

void CALLBACK OpenLogConsole() 
{
	// create the console 
	AllocConsole();
	// move standart cout stream to Allocated console (For all application include libraries)
	HANDLE stdHandle;
	int hConsole;
	FILE* fp;
	stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	hConsole = _open_osfhandle((intptr_t)stdHandle, _O_TEXT);
	fp = _fdopen(hConsole, "w");

	freopen_s(&fp, "CONOUT$", "w", stdout);

}


void LogToConsole(string text)
{
	std::cout<<text<<std::endl;
}
#endif

#ifdef RAD_MESSAGEBOX

void RadMessageBox(string title, string msg, EMessageBoxType type)
{
#if defined(USE_PLATFORM_WINDOWS)
    if(type == eBT_Information)
        MessageBoxA(NULL, msg.c_str(), title.c_str(), MB_OK | MB_ICONERROR);
	else if (type == eBT_Error)
	{
		MessageBoxA(NULL, msg.c_str(), title.c_str(), MB_OK | MB_ICONERROR);
		throw std::runtime_error(msg);
	}
    else if (type == eBT_Warning)
         MessageBoxA(NULL, msg.c_str(), title.c_str(), MB_OK | MB_ICONERROR);
#elif defined(USE_PLATFORM_LINUX)
    if(type == eBT_Information)
        system(string("zenity --info --text=\"" + msg + "\" --title=\"" + title + "\"").c_str());
	else if (type == eBT_Error)
	{
		system(string("zenity --error --text=\"" + msg + "\" --title=\"" + title + "\"").c_str());
		throw std::runtime_error(msg);
	}
    else
        system(string("zenity --warning --text=\"" + msg + "\" --title=\"" + title + "\"").c_str());
#endif

}


#endif