/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 18/01/2019  19:48 : Created by Stanislav Migunov
*************************************************************************/
#ifndef __RFINFO_H__
#define __RFINFO_H__

#define ENGINE_NAME "RADFOX Engine"

#define ENGINE_VERSION_MAJOR 0
#define ENGINE_VERSION_MINOR 3
#define ENGINE_VERSION_BUILD 175

#define ENGINE_GAME_TITLE "Launcher"

#define GAME_VERSION_MAJOR 0
#define GAME_VERSION_MINOR 0

#endif
