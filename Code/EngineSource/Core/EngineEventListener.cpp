/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 13/02/2019  11:19 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "EngineEventListener.h"

CEngineEvent::CEngineEvent()
{
	m_pEngineEventListenersBuff.empty();
	m_engineEventBuff = EVENT_INVALID;
}

CEngineEvent::~CEngineEvent()
{
	if (m_pEngineEventListenersBuff.size() > 0)
	{
		for (auto& it : m_pEngineEventListenersBuff)
		{
			RemoveListener(it);
		}
	}

	m_pEngineEventListenersBuff.empty();
}

void CEngineEvent::AddListener(IEngineEventListener * pEngineEvents)
{
	m_pEngineEventListenersBuff.push_back(pEngineEvents);
}

void CEngineEvent::RemoveListener(IEngineEventListener * pEngineEvents)
{
	find_and_erase(m_pEngineEventListenersBuff, pEngineEvents);
}

void CEngineEvent::EngineEvent(EEngineEvents event)
{
	for (auto& it : m_pEngineEventListenersBuff)
	{
		it->OnEngineEvent(event);
	}
}
