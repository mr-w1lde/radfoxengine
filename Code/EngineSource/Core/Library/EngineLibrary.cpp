/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 02/02/2019  18:52 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "EngineLibrary.h"




CEngineLibrary::CEngineLibrary()
{
	Initialize();
}

CEngineLibrary::~CEngineLibrary()
{
}

void CEngineLibrary::Initialize()
{
	m_engineLibraries.clear();
}

void CEngineLibrary::Release()
{
	for (auto& it : m_engineLibraries)
	{
		it.m_pEngineLibraries->Release();
		RadLibraryFree(it.rdLibrary);
	}

	m_engineLibraries.clear();

	this->~CEngineLibrary();
}

void * CEngineLibrary::LoadUnknownLibrary(const char* lib, SInitializeParams& params)
{
	CoreLog("Loading %s Library...", lib);

	SUnkownLibrary loadingLib;

	loadingLib.rdLibrary = RadLoadLibrary(lib);

	assert(loadingLib.rdLibrary, nullptr, "Loading Library is nullptr...");

	IUnknownLibrary::TEntryPoint epCreateUnknownInterface = RadProcAddress<IUnknownLibrary::TEntryPoint>(loadingLib.rdLibrary, "CreateUknownLibraryStartup");

	assert(epCreateUnknownInterface, nullptr, "Couldn't Create Interface...");

	loadingLib.m_pEngineLibraries = epCreateUnknownInterface();

	assert(loadingLib.m_pEngineLibraries, nullptr, "Couldn't Initialize Library...");

	m_engineLibraries.push_back(loadingLib);

	loadingLib.m_pEngineLibraries->RegisterEnv(params);

	return loadingLib.m_pEngineLibraries;
}
