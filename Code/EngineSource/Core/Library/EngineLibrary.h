/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 02/02/2019  18:52 : Created by Stanislav Migunov
*************************************************************************/

#include <Core/Library/IEngineLibrary.h>
#include <UnkownLibrary/IUnknownLibrary.h>

#include <RFLibrary.h>

#ifndef _ENGINE_LIBRARY_H__
#define _ENGINE_LIBRARY_H__

struct SUnkownLibrary
{
	IUnknownLibrary* m_pEngineLibraries;
	RadDynamic rdLibrary;
};

class CEngineLibrary : public IEngineLibrary
{
	using UnkownArray = std::vector<SUnkownLibrary>;
public:
	CEngineLibrary();
	~CEngineLibrary();

public:
	virtual void 	Initialize() override;
	virtual void 	Release() override;
	virtual void* 	LoadUnknownLibrary(const char* lib, SInitializeParams& params) override;
	
public:
	UnkownArray GetUnknownLibs() { return m_engineLibraries; }

private:
	UnkownArray m_engineLibraries;
};



#endif