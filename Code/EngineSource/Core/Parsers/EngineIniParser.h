/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 03/02/2019  17:19 : Created by Stanislav Migunov
*************************************************************************/

#include <Core/Parsers/IEngineIniParser.h>
#include "inih/INIReader.h"

#ifndef _ENGINE_INIPARSER_H__
#define _ENGINE_INIPARSER_H__

class CInih : public IInih
{
public:
	CInih(const char* path);
	~CInih();

public:

	virtual int GetParsError() override;

	virtual std::string GetString(std::string section,
		std::string name,
		std::string default_value) override;

	virtual long GetInteger(std::string section, std::string name, long default_value) override;
	
	virtual double GetReal(std::string section, std::string name, double default_value) override;
	
	virtual bool GetBoolean(std::string section, std::string name, bool default_value) override;

	virtual std::set<std::string> GetSections() const override;

	virtual std::set<std::string> GetFields(std::string section) const override;
	//~IInih

private:
	INIReader* m_pReader;
};

class CEngineIniParser : public IEngineIniParser
{
public:
	CEngineIniParser();
	~CEngineIniParser();

public:
	// IEngineIniParser
	virtual void Initialize() override;
	virtual IInih* LoadFile(const char* path) override;
	//~IEngineIniParser
};

#endif