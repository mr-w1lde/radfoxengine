/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 03/02/2019  17:19 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "EngineIniParser.h"

CEngineIniParser::CEngineIniParser()
{
	Initialize();
}

CEngineIniParser::~CEngineIniParser()
{
}

void CEngineIniParser::Initialize()
{
}

IInih* CEngineIniParser::LoadFile(const char* path)
{
	string rootPath = string(gEngine->pCore->GetRootDir()) + "/" + path;
	IInih* pInih = new CInih(path);

	if (pInih->GetParsError() == -1)
	{
		CoreLogError("[Ini] Couldn't open %s file.", path);
	}

	return pInih;
}

CInih::CInih(const char* path)
{
	m_pReader = new INIReader(path);
}

CInih::~CInih()
{
	SAFE_DELETE(m_pReader);
}

int CInih::GetParsError()
{
	return m_pReader->ParseError();
}

std::string CInih::GetString(std::string section, std::string name, std::string default_value)
{
	return m_pReader->Get(section, name, default_value);
}

long CInih::GetInteger(std::string section, std::string name, long default_value)
{
	return m_pReader->GetInteger(section, name, default_value);
}

double CInih::GetReal(std::string section, std::string name, double default_value)
{
	return m_pReader->GetReal(section, name, default_value);
}

bool CInih::GetBoolean(std::string section, std::string name, bool default_value)
{
	return m_pReader->GetBoolean(section, name, default_value);
}

std::set<std::string> CInih::GetSections() const
{
	return m_pReader->GetSections();
}

std::set<std::string> CInih::GetFields(std::string section) const
{
	return m_pReader->GetFields(section);
}
