/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 04/02/2019  03:31 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _ENGINE_CVAR_H__
#define _ENGINE_CVAR_H__

#include <Core/Console/ICVar.h>

class CEngineCVar : public ICVar
{
public:
	CEngineCVar(SCVarId var, TVarValue value, string help);
	~CEngineCVar();

public:
	// ICVar
	virtual void Release() override;

	virtual const char* GetCurrentSection() override;
	virtual const char* GetTitle();
	virtual const char* GetHelp() override;

	virtual const char* GetValString() override;
	virtual int GetValInt() override;
	virtual bool GetValBool() override;
	virtual float GetValFloat() override;

	virtual void SetVal(TVarValue value) override;

	const virtual std::type_info& GetType() const override;
	//~ICVar

private:
	TVarValue m_value;
	string m_currSection;
	string m_sHelp;
	string m_sTitle;
};

#endif