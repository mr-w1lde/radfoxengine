/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 15/02/2019  06:46 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "EngineCFunc.h"

CEngineCFunc::CEngineCFunc(const char * title, IConsoleFunc::ConsoleFunctionPtr func)
{
	m_title = title;
	m_func = func;
}

CEngineCFunc::~CEngineCFunc()
{
	m_func = nullptr;
}

void CEngineCFunc::Call(IConsoleFuncArgs * pArgs)
{
	gEngine->pLog->Log("[CMD] Executing %s command...", m_title.c_str());
	(*m_func)(pArgs);
}

