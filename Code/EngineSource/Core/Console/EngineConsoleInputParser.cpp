﻿/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 15/02/2019  10:02 : Created  by Denis beloedoff
*************************************************************************/

#include "StdAfx.h"
#include "EngineConsoleInputParser.h"

#include "EngineCFunc.h"

void CEngineConsoleInputParser::Parse(string input)
{

	std::smatch matches;
	std::regex variablesShow("^([A-z]+)::([A-z]+)$");																// String pattern for variables show
	std::regex variableHelpShow("^help ([A-z]+)::([A-z]+)$");														// String pattern for variables show
	std::regex variablesSet("^([A-z]+)::([A-z]+) {0,1}= {0,1}([A-z0-9А-я.]+|\"[A-z0-9А-я. ]*\"+)$");				// String pattern for variables set
	std::regex functionsCall("^call ([A-z0-9]+)[ ]*([A-z0-9А-я ]+)*$");												// String pattern for functions call
	std::regex argumentsSearch("(?:[A-z0-9А-я]+)+");																// String pattern for functions args

	ICVar* pCVar;
	if (std::regex_search(input, matches, variablesShow))
	{
		matches.str(1);			// Sectrion
		matches.str(2);			// Var

		pCVar = gEngine->pConsole->GetCVar(SCVarId(matches.str(1).c_str(), matches.str(2).c_str()));
		ShowVar(pCVar);
	}
	else if (std::regex_search(input, matches, variableHelpShow))
	{
		matches.str(1);			// Sectrion
		matches.str(2);			// Var

		pCVar = gEngine->pConsole->GetCVar(SCVarId(matches.str(1).c_str(), matches.str(2).c_str()));
		gEngine->pLog->Log("[Console] %s", pCVar->GetHelp());

	}
	else if (std::regex_search(input, matches, variablesSet))
	{
		matches.str(1);			// Sectrion
		matches.str(2);			// Var
		matches.str(3);			// Value

		pCVar = gEngine->pConsole->GetCVar(SCVarId(matches.str(1).c_str(), matches.str(2).c_str()));

		if (!pCVar)
			return;

		string dataToSet = matches.str(3);

		if (dataToSet.find('\"') > -1)
		{
			dataToSet = dataToSet.erase(0);
			dataToSet = dataToSet.erase(dataToSet.size());
		}

		switch (GetUserInputType(dataToSet))
		{
		case eBool:
		{
			bool tmp;

			if (dataToSet == "true")
				tmp = true;

			if (dataToSet == "false")
				tmp = false;

			pCVar->SetVal((bool)tmp);
			break;
		}
		case eInt:
		{
			int tmp;
			tmp = std::atoi(dataToSet.c_str());
			pCVar->SetVal((int)tmp);
			break;
		}
		case eString:
		{
			pCVar->SetVal((string)dataToSet);
			break;
		}
		}

	}
	else if (std::regex_search(input, matches, functionsCall))
	{
		matches.str(1);			// Function
		if (matches.size() > 0)
		{
			for (auto match : matches)
			{
				match.str();	//	Argument
			}
		}
		else
		{
			// no args
		}

		CallFunc(matches.str(1), nullptr);
	}
	else
	{
		gEngine->pLog->LogWarning("[Console] Unrecognized command. %s", input.c_str());
		gEngine->pLog->Log("[Console] To show a variable: SectionName::VariableName \n To set a variable: SectionName::VariableName = YourData/\"Your data\" \n To call a function: call FunctionName <Args...> \n clear for screen clearing");
	}
}

void CEngineConsoleInputParser::ShowVar(ICVar* pVar)
{
	if (!pVar)
		return;

	if (pVar->GetType() == TID_BOOL)
	{
		gEngine->pLog->Log("[CVAR] %s::%s = %s", pVar->GetCurrentSection(), pVar->GetTitle(), pVar->GetValBool() ? "true" : "false");
	}
	else if (pVar->GetType() == TID_FLOAT)
	{
		gEngine->pLog->Log("[CVAR] %s::%s = %f", pVar->GetCurrentSection(), pVar->GetTitle(), pVar->GetValFloat());
	}
	else if (pVar->GetType() == TID_INT)
	{
		gEngine->pLog->Log("[CVAR] %s::%s = %u", pVar->GetCurrentSection(), pVar->GetTitle(), pVar->GetValInt());
	}
	else if (pVar->GetType() == TID_STRING)
	{
		gEngine->pLog->Log("[CVAR] %s::%s = %s", pVar->GetCurrentSection(), pVar->GetTitle(), pVar->GetValString());
	}
}

void CEngineConsoleInputParser::EditVar(ICVar* pVar)
{

}

void CEngineConsoleInputParser::CallFunc(string func, const char* args)
{
	CEngineConsole* m_pEngineConsole = static_cast<CEngineConsole*>(gEngine->pConsole);

	if (!m_pEngineConsole)
		return;

	for (auto& it : m_pEngineConsole->GetEngineFunc())
	{
		if (it->GetTitle() == string(func))
		{
			it->Call(nullptr);
			break;
		}
	}
}

EInputTypes CEngineConsoleInputParser::GetUserInputType(string input)
{
	if (input == "true" || input == "false")
	{
		return eBool;
	}

	bool isDigit = false;
	bool isFloat = false;

	for (char ch : input)
	{
		isDigit = std::isdigit(ch, std::locale());
	}
	if (isFloat)
	{
		return eFloat;
	}
	else if (isDigit)
	{
		return eInt;
	}
	else
	{
		return eString;
	}

}
