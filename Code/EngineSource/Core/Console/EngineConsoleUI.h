/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 04/02/2019  19:02 : Created  by Denis Beloedoff
*************************************************************************/
#ifndef _ENGINE_CONSOLE_UI_H__
#define _ENGINE_CONSOLE_UI_H__

#pragma warning( push )
#pragma warning (disable : 4005) //C4005 - Safety for our app.

#define	HEADER_COLOR		1
#define INPUT_COLOR			2
#define LOG_COLOR			12
#define ERROR_COLOR			13
#define	SUCCESS_COLOR		14
#define	WARNING_COLOR		15
#define FATALERROR_COLOR	16
#define CURSOR_COLOR		17

#include "Engine/enginecore.h"

#include <string>
#include <cstring>
#include <thread>
#include <vector>

#include "Core/ILogSystem.h"

#include <panel.h>

#ifdef USE_PLATFORM_WINDOWS 
	#include <curses.h>
	#include <windows.h>
	#define KEY_BACKSPACE 8
#endif
#ifdef USE_PLATFORM_LINUX
	#include <ncurses.h>
	#include <unistd.h>
#endif

class CEngineConsole;

class CEngineConsoleUI : public ILogEventCallback
{

public:
	CEngineConsoleUI();
	~CEngineConsoleUI();

public:
	//ILogEventCallback
	virtual void OnLogWrite(const char* msg, ELogType type);
	//~ILogEventCallback

public:
	void RunConsole();
	void StopConsole();

private:
	int						_historyPos				= -1;
	int						_scroll					= 0;
	bool					_isRun					= true;
	std::string				_consoleInputBuffer		= "";
	PANEL*					_pHistoryPanel;
	PANEL*					_pHeaderPanel;
	PANEL*					_pInputPanel;
	WINDOW*					_pHistoryWindow;
	WINDOW*					_pHeaderWindow;
	WINDOW*					_pInputWindow;
	std::vector<string>		_inputCache;

private:
	CEngineConsole* m_pEngineConsole;
};

#pragma warning( pop )

#endif