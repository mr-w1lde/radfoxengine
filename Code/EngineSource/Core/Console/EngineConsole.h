/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 04/02/2019  03:45 : Created by Stanislav Migunov
*************************************************************************/
#include <Core/Console/IConsole.h>
#include "EngineConsoleUI.h"


#ifndef _ENGINE_CONSOLE_H__
#define _ENGINE_CONSOLE_H__


struct SCVarArray
{
private:
	const char* sSection;

public:
	std::vector<ICVar*> pSectionVar;

public:
	SCVarArray(const char* section)
	{
		sSection = section;
		pSectionVar.clear();
	}

	const char* GetSection() { return sSection; }
};

class CEngineCFunc;
class CEngineConsoleInputParser;

class CEngineConsole : public IConsole
{
public:
	CEngineConsole();
	~CEngineConsole();

public:
	// IConsole
	virtual void Initialize() override;
	virtual void Release() override;

	virtual bool RegisterSection(const char* id) override;
	virtual bool RegisterVar(SCVarId id, TVarValue value, const char* help) override;

	virtual void RegisterFunction(const char* title, IConsoleFunc::ConsoleFunctionPtr function) override;

	virtual void RemoveSection(const char* id) override;
	virtual void RemoveVar(SCVarId id) override;

	virtual ICVar* GetCVar(SCVarId id) override;
	//~IConsole

public:
	std::vector<SCVarArray*> GetEngineVars() const { return m_engineVars; }
	std::vector<CEngineCFunc*> GetEngineFunc() const { return m_engineFuncs; }
	void ParseInput(string msg);
private:
	CEngineConsoleUI* m_pUIConsole;
	CEngineConsoleInputParser* m_pParserConsole;
	std::vector<SCVarArray*> m_engineVars;
	std::vector<CEngineCFunc*> m_engineFuncs;
};

#endif