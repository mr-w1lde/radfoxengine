/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 15/02/2019  10:02 : Created  by Denis beloedoff
*************************************************************************/

#ifndef _ENGINE_CONSOLE_INPUT_PARSER_H__
#define _ENGINE_CONSOLE_INPUT_PARSER_H__

#include <regex>
#include "EngineConsole.h"

enum EInputTypes
{
	eInt = 0, eFloat, eBool, eString
};

class CEngineConsoleInputParser
{
public:
	CEngineConsoleInputParser() {};
	~CEngineConsoleInputParser() {};
public:
	void Parse(string input);
private:
	void ShowVar(ICVar* pVar);
	void EditVar(ICVar* pVar);
	void CallFunc(string func, const char* args);
	EInputTypes GetUserInputType(string input);
};

#endif