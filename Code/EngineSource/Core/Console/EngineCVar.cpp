/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 04/02/2019  03:31 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "EngineCVar.h"

#include <boost/variant.hpp>

CEngineCVar::CEngineCVar(SCVarId var, TVarValue value, string help)
{
	m_value = value;
	m_currSection = var.GetSection();
	m_sTitle = var.GetVar();
	m_sHelp = help;
}

CEngineCVar::~CEngineCVar()
{
}

void CEngineCVar::Release()
{
	this->~CEngineCVar();
}

const char * CEngineCVar::GetCurrentSection()
{
	return m_currSection.c_str();
}

const char * CEngineCVar::GetTitle()
{
	return m_sTitle.c_str();
}


const char * CEngineCVar::GetHelp()
{
	return m_sHelp.c_str();
}

const char * CEngineCVar::GetValString()
{
	return boost::get<string>(m_value).c_str();
}

int CEngineCVar::GetValInt()
{
	return boost::get<int>(m_value);
}

bool CEngineCVar::GetValBool()
{
	return boost::get<bool>(m_value);
}

float CEngineCVar::GetValFloat()
{
	return boost::get<float>(m_value);
}

void CEngineCVar::SetVal(TVarValue value)
{
	if (m_value.type() != value.type())
	{
		CoreLogError("[CVAR] Trying to rewrite of type the console variable<%s::%s>.", m_currSection.c_str(), m_sTitle.c_str());
		return;
	}

	m_value = value;
}

const std::type_info & CEngineCVar::GetType() const
{
	return m_value.type();
}
