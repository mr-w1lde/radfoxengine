/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 04/02/2019  03:31 : Created by Stanislav Migunov
*************************************************************************/

#include <Core/Console/ICFunc.h>
#include <vector>

#ifndef _ENGINE_CFUNC_H__
#define _ENGINE_CFUNC_H__

class CEngineCFunc
{
public:
	CEngineCFunc(const char* title, IConsoleFunc::ConsoleFunctionPtr func);
	~CEngineCFunc();
public:
	void Call(IConsoleFuncArgs* pArgs);
	string GetTitle() { return m_title; }
private:
	IConsoleFunc::ConsoleFunctionPtr m_func;
	string m_title;
};

class CConsoleFuncArgs : public IConsoleFuncArgs
{
public:
	typedef std::vector<const char*> FuncArgs;
public:
	CConsoleFuncArgs(FuncArgs args, const char* line)
	{
		m_args = args;
		m_line = line;
	}
	~CConsoleFuncArgs()
	{
		m_args.clear();
		SAFE_DELETE(m_line);
	}

public:
	// IConsoleFuncArgs
	virtual int GetArgsCount() override { return m_args.size(); }
	virtual const char* GetArg(int index) override { return m_args[index]; }
	virtual const char* GetLine() override { return m_line; }
	//~IConsoleFuncArgs

private:
	const char* m_line;
	FuncArgs m_args;
};

#endif