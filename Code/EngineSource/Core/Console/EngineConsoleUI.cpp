/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 04/02/2019  19:02 : Created  by Denis Beloedoff
*************************************************************************/
#include "StdAfx.h"
#include "EngineConsoleUI.h"

#include "EngineConsole.h"
#include "EngineCFunc.h"
#include "EngineCVar.h"


CEngineConsoleUI::CEngineConsoleUI()
{

	RunConsole();
	gEngine->pLog->AddListener(this);
}

CEngineConsoleUI::~CEngineConsoleUI()
{
	gEngine->pLog->RemoveListener(this);
	StopConsole();
}

void CEngineConsoleUI::OnLogWrite(const char * msg, ELogType type)
{
	switch (type)
	{
		case ELogType::eLT_FatalError:
		{	
			wattron(_pHistoryWindow, COLOR_PAIR(FATALERROR_COLOR));
			wattron(_pHistoryWindow, A_BLINK);
			break;
		}
		case ELogType::eLT_Log:
		{
			wattron(_pHistoryWindow, COLOR_PAIR(LOG_COLOR));
			break;
		}
		case ELogType::eLT_LogError:
		{
			wattron(_pHistoryWindow, COLOR_PAIR(ERROR_COLOR));
			break;
		}
		case ELogType::eLT_LogSuccess:
		{
			wattron(_pHistoryWindow, COLOR_PAIR(SUCCESS_COLOR));
			break;
		}
		case ELogType::eLT_LogWarning:
		{
			wattron(_pHistoryWindow, COLOR_PAIR(WARNING_COLOR));
			break;
		}
	}
	waddstr(_pHistoryWindow, string(string(msg) + "\n").c_str());
	wattroff(_pHistoryWindow,COLOR_PAIR(LOG_COLOR)| COLOR_PAIR(ERROR_COLOR) | COLOR_PAIR(SUCCESS_COLOR)|COLOR_PAIR(WARNING_COLOR) | COLOR_PAIR(FATALERROR_COLOR));
	wattroff(_pHistoryWindow, A_BLINK);

}


void CEngineConsoleUI::RunConsole()
{
	int _row = 0, _col = 0;

	initscr();
	start_color();
	cbreak();
	noecho();
	curs_set(0);
	keypad(stdscr, true);

	if (!has_colors()) 
	{
		endwin();
		CoreLogWarning("Your terminal does not support color!");
		
	}

	init_pair(FATALERROR_COLOR, COLOR_BLACK, COLOR_RED);
	init_pair(LOG_COLOR, COLOR_WHITE, COLOR_BLACK);
	init_pair(ERROR_COLOR, COLOR_RED, COLOR_BLACK);
	init_pair(SUCCESS_COLOR, COLOR_GREEN, COLOR_BLACK);
	init_pair(WARNING_COLOR, COLOR_YELLOW, COLOR_BLACK);
	init_pair(HEADER_COLOR, COLOR_BLACK, COLOR_WHITE);
	init_pair(INPUT_COLOR, COLOR_BLACK, COLOR_CYAN);
	init_pair(CURSOR_COLOR, COLOR_WHITE, COLOR_CYAN);

	getmaxyx(stdscr, _row, _col);



	//init windows
	_pHeaderWindow = newwin(1, _col, 0, 0);
	wbkgdset(_pHeaderWindow, COLOR_PAIR(HEADER_COLOR));
	wclear(_pHeaderWindow);
	wrefresh(_pHeaderWindow);

	_pHistoryWindow = newpad(256, _col);		//Pad - window with buffer
	scrollok(_pHistoryWindow, true);

	_pInputWindow = newwin(1, _col-1, _row - 1,1);
	wbkgdset(_pInputWindow, COLOR_PAIR(INPUT_COLOR));
	wclear(_pInputWindow);
	wrefresh(_pInputWindow);
	//end init windows

	_pHeaderPanel = new_panel(_pHeaderWindow);
	_pHistoryPanel = new_panel(_pHistoryWindow);
	_pInputPanel = new_panel(_pInputWindow);

	char* _headmesg = (char*)malloc(_col);

#pragma warning (push)
#pragma warning (disable : 4996) //C4996 - Safety in this case.

	sprintf(_headmesg, "%s v%d.%d.%d: %s v%d.%d", ENGINE_NAME, ENGINE_VERSION_MAJOR, ENGINE_VERSION_MINOR, ENGINE_VERSION_BUILD,
		ENGINE_GAME_TITLE, GAME_VERSION_MAJOR, GAME_VERSION_MINOR);

#pragma warning (pop)

	attron(COLOR_PAIR(CURSOR_COLOR));
	mvprintw(_row -1, 0, ">");
	attroff(COLOR_PAIR(CURSOR_COLOR));
	
	mvwprintw(_pHeaderWindow, 0, ((_col - std::size<std::string>(_headmesg)) / 2), _headmesg);

	new std::thread
	(
		([_row, _col, _headmesg, this]
		{
			while (_isRun)
			{
				wclear(_pInputWindow);

				wprintw(_pInputWindow, _consoleInputBuffer.c_str());

				update_panels();

				halfdelay(1);
				int inputChar = getch();

				switch (inputChar)
				{
					case KEY_BACKSPACE:
					case KEY_DC:
					case 127:
					{
						if (_consoleInputBuffer.length() != 0)
							_consoleInputBuffer.resize(_consoleInputBuffer.length() - 1);
						break;
					}

					case ERR:
						break;

					case KEY_UP:
					{
						if (_historyPos <= 0)
						{
							break;
						}
						else
						{
							_historyPos--;
							_consoleInputBuffer = _inputCache.at(_historyPos);
							break;
						}
					}

					case KEY_DOWN:
					{
						if (_historyPos >= (int)_inputCache.size() - 1)
						{
							break;
						}
						else
						{
							_historyPos++;
							_consoleInputBuffer = _inputCache.at(_historyPos);
							break;
						}
					}
					case KEY_ENTER:
					case 13:
					case 10:
					{

						if (_consoleInputBuffer.length() > 0)
						{
							//waddstr(_pHistoryWindow, string(_consoleInputBuffer + "\n").c_str());

							_inputCache.push_back(_consoleInputBuffer);

							_historyPos = (int)_inputCache.size();

							m_pEngineConsole = static_cast<CEngineConsole*>(gEngine->pConsole);

							if (_consoleInputBuffer == "clear")
							{
								wclear(_pHistoryWindow);
							}
							else
							{
								m_pEngineConsole->ParseInput(_consoleInputBuffer);
							}
							_consoleInputBuffer = "";

						}

						//call - call some func.
						//set - set the val of var.
						//get - get curret val of var.
						///Example: 
						//Player::Speed = 0.25
						//Render::Width
						//call SomeFunc __va_args__

						if (_scroll + _row <= (*_pHistoryWindow)._cury )			// If presed return and end of history out of bounds - go to history
						{
							_scroll = (*_pHistoryWindow)._cury-(_row - 2);
						}
						if ((*_pHistoryWindow)._cury - _scroll >= _row - 1)		// Scroll pad if text is out of bounds
						{
							_scroll++;
						}

						break;
					}
					case KEY_HOME:
					{
						if (_scroll > 0)
							_scroll--;

						break;
					}
					case KEY_END:
					{
						if (_scroll < (*_pHistoryWindow)._cury - (_row-2))
							_scroll++;

						break;
					}
					default:
					{
						_consoleInputBuffer += (int)inputChar;
						
						break;
					}
				}
				if (_scroll >= 65535)
				{
					_scroll = 0;
				}
				if (_inputCache.max_size() <= _inputCache.size() + 1)
				{
					_inputCache.erase(_inputCache.begin(), _inputCache.begin() + 1);
				}
				pnoutrefresh(_pHistoryWindow, 0 + _scroll, 0, 1, 0,_row-2,_col-1);
				doupdate();
			}

			endwin();
		})

	);
}

void CEngineConsoleUI::StopConsole()
{
	_isRun = false;
}

