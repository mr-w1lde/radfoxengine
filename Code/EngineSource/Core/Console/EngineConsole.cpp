/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 04/02/2019  03:45 : Created  by Stanislav Migunov
- 04/02/2019  18:15 : Modified by Denis beloedoff
*************************************************************************/
#include "StdAfx.h"
#include "EngineConsole.h"
#include "EngineCVar.h"
#include "EngineCFunc.h"
#include "EngineConsoleInputParser.h"

CEngineConsole::CEngineConsole()
{
	CoreLog("Initializing Engine Console...");
	Initialize();
}

CEngineConsole::~CEngineConsole()
{
	for (auto& it : m_engineVars)
	{
		if (it->pSectionVar.size() > 0)
		{
			for (ICVar* var : it->pSectionVar)
			{
				var->Release();
			}
		}
	}

	m_engineVars.clear();
	m_engineFuncs.clear();

	m_pUIConsole->StopConsole();
	SAFE_DELETE(m_pUIConsole);
}

void CEngineConsole::Initialize()
{
	m_engineVars.clear();
	m_pParserConsole = new CEngineConsoleInputParser();
	m_pUIConsole = new CEngineConsoleUI();
	m_pUIConsole->RunConsole();
}

void CEngineConsole::Release()
{
	CoreLog("Releasing Engine Console...");
	this->~CEngineConsole();
}

bool CEngineConsole::RegisterSection(const char * id)
{
	SCVarArray* pRegisterArray = new SCVarArray(id);
	
	m_engineVars.push_back(pRegisterArray);

	return pRegisterArray != nullptr ? true : false;
}

bool CEngineConsole::RegisterVar(SCVarId id, TVarValue value, const char* help)
{
	bool bResult = false;

	for (auto& it : m_engineVars)
	{
		if (string(it->GetSection()) == string(id.GetSection()))
		{
			it->pSectionVar.push_back(new CEngineCVar(id, value, help));
			bResult = true;
			break;
		}
	}

	if (!bResult)
	{
		CoreLogError("[Console] Coudldn't register %s:%s", id.GetSection(), id.GetVar());
	}

	return bResult;
}

void CEngineConsole::RegisterFunction(const char * title, IConsoleFunc::ConsoleFunctionPtr function)
{
	m_engineFuncs.push_back(new CEngineCFunc(title, function));
}

void CEngineConsole::RemoveSection(const char * id)
{
	bool bFound = false;

	for (auto& it : m_engineVars)
	{
		if (string(it->GetSection()) == string(id))
		{
			if (it->pSectionVar.size() > 0)
			{
				for (ICVar* var : it->pSectionVar)
				{
					var->Release();
				}
			}

			bFound = true;
			SAFE_DELETE(it);
			break;
		}
	}

	if (!bFound)
	{
		CoreLogError("[Console] Couldn't delete %s section", id);
	}
}

void CEngineConsole::RemoveVar(SCVarId id)
{
	bool bFound = false;

	for (auto& it : m_engineVars)
	{
		if (string(id.GetSection()) == string(it->GetSection()))
		{
			for (auto& var : it->pSectionVar)
			{
				if (string(var->GetTitle()) == (id.GetVar()))
				{
					SAFE_DELETE(var);
					bFound = true;
					break;
				}
			}
		}
	}

	if (!bFound)
	{
		CoreLogError("[Console] Couldn't delete %s:%s var...", id.GetSection(), id.GetVar());
	}
}

ICVar * CEngineConsole::GetCVar(SCVarId id)
{
	ICVar* pCVar = nullptr;

	for (auto& it : m_engineVars)
	{
		if (string(id.GetSection()) == string(it->GetSection()))
		{
			for (auto& var : it->pSectionVar)
			{
				if (string(var->GetTitle()) == string(id.GetVar()))
				{
					pCVar = var;
					break;
				}
			}
		}
	}

	if (!pCVar)
	{
		CoreLogError("[Console] Couldn't find %s:%s var...", id.GetSection(), id.GetVar());
	}

	return pCVar;
}

void CEngineConsole::ParseInput(string msg)
{
	m_pParserConsole->Parse(msg);
}
