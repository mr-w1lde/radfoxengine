/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 13/02/2019  11:19 : Created by Stanislav Migunov
*************************************************************************/
#include <Core/IEngineCore.h>

#ifndef _ENGINE_EVENT_LISTENER_H__
#define _ENGINE_EVENT_LISTENER_H__

class CEngineEvent : public IEngineEvent
{
public:
	CEngineEvent();
	~CEngineEvent();

public:
	// IEngineEvent
	virtual void AddListener(IEngineEventListener* pEngineEvents) override;
	virtual void RemoveListener(IEngineEventListener* pEngineEvents) override;

	virtual void EngineEvent(EEngineEvents event) override;
	//~IEngineEvent

private:
	EEngineEvents m_engineEventBuff;
	std::vector<IEngineEventListener*> m_pEngineEventListenersBuff;
};

#endif