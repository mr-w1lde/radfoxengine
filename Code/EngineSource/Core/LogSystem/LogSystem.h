/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 18/01/2019  18:35 : Created by Stanislav Migunov
*************************************************************************/
#include <Core/ILogSystem.h>
#include <rlutil/Include/rlutil.h>
#include <fstream>

#ifndef _LOGSYSTEM_H__
#define _LOGSYSTEM_H__

enum ELogColors
{
	eLC_BLUE = 1,
	eLC_GREEN,
	eLC_CYAN,
	eLC_RED,
	eLC_MAGENTA,
	eLC_BROWN,
	eLC_GREY,
	eLC_DARKGREY,
	eLC_LIGHTBLUE,
	eLC_LIGHTGREEN,
	eLC_LIGHTCYAN,
	eLC_LIGHTRED,
	eLC_LIGHTMAGENTA,
	eLC_YELLOW,
	eLC_WHITE
};


struct SystemInfo
{
	string m_ProcessorName;
	string m_ProcessorCores;
	string m_ProcessorArchitecture;
	string m_OS_NamePC;
	string m_OS_NameUser;
	string m_OS_Version;
	string m_OS_BuildVersion;

	std::vector <std::wstring> m_VideoDrivesName;
	std::vector <std::string> m_VideoDrivesMemory;

	string m_Ram;
	string m_ScreenResolution;
};

class CWriteToLogFile : public ILogEventCallback
{
public:
	CWriteToLogFile(string logFileName);
	~CWriteToLogFile();

	void OnLogWrite(const char* msg, ELogType type) override;

private:
	void GetTime();
	void GetSystemInfo();
	void WriteSystemInfoToFile();

private:
	tm* m_pTm;
	SystemInfo* m_pSystemInfo;
	string m_currentTime;
	string m_currentDate;
	string m_path;
};

class CLogSystem : public ILogSystem
{
public:
	CLogSystem();
	~CLogSystem();
public:
	void Initialize();
public:
	// ILogSystem
	virtual void Log(const char* msg, ...) PRINTF_PARAMS(2, 3) override;
	virtual void LogError(const char* msg, ...) PRINTF_PARAMS(2, 3) override;
	virtual void LogWarning(const char* msg, ...) PRINTF_PARAMS(2, 3) override;
	virtual void LogSuccess(const char* msg, ...) PRINTF_PARAMS(2, 3) override;
	virtual void FatalError(const char* msg) override;

	virtual void AddListener(ILogEventCallback* listener) override;
	virtual void RemoveListener(ILogEventCallback* listener) override;
	//~ILogSystem
public:
	void SetLogFileName(const char* title) { m_logFileName = title; }

protected:
	void WriteConsoleLog(string msg, va_list args, ELogType logType);

private:
	std::vector<string> m_logBuffer;
	std::vector<ILogEventCallback*> m_logCallbackListener;
	CWriteToLogFile* m_pWriteLogToFile;
	string m_logFileName;
};
#endif