/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 18/01/2019  18:40 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "Core.h"

extern "C"
{
	RADCORE_API IEngineCore* CreateCoreDllStartup()
	{
		return new CEngineCore();
	}
}

