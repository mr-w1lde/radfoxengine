/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 01/02/2019  12:50 : Created by Stanislav Migunov
*************************************************************************/

#include <Core/IEngineCore.h>
#include <Render/Null/IRenderNull.h>
#include <RFLibrary.h>

#ifndef _CORE_H__
#define _CORE_H__

class CLogSystem;
class CEngineLibrary;
class CEngineIniParser;
class CEngineConsole;
class CEngineEvent;

class CEngineCore : public IEngineCore
{
public:
	CEngineCore();
	virtual ~CEngineCore();
public:
	// ISystem
	virtual void						Initialize(SInitializeParams& params) override;
	virtual bool						CompleteInit() override;
	virtual void						Release() override;
	virtual ICoreGlobalVariable*		GetGlobalVariable() override;
	virtual const char*					GetRootDir() override { return m_rootDir.c_str(); }
	virtual void                        RunCoreLoop() override;
	virtual void						Quit() override;
	virtual void                        IsQuiting() override;
	virtual ILogSystem*					GetILogSystem() override;
	virtual IEngineLibrary*             GetIIEngineLibrary() override;
	virtual IEngineIniParser*			GetIEngineIniParser() override;
	virtual IEngineEvent*				GetIEngineEvent() override;
	//~ISystem

protected:
	void LoadEngineLibraries(SInitializeParams& params);
	void CoreLoop(float frameTime);

private:
	CLogSystem* m_pLogSystem;
	CEngineLibrary* m_pEngineLibrary;
	CEngineIniParser* m_pEngineIniParser;
	CEngineConsole* m_pEngineConsole;
	CEngineEvent* m_pEngineEvent;

private:
	string m_rootDir;

private:
	RadDynamic m_rdRenderNullLibrary;
	IRenderNull* m_pRenderNull;

private:
	SInitializeParams* m_pInitParams;
	bool m_bIsInitComplete;

private:
	bool m_bIsCoreLoop;
};

#endif