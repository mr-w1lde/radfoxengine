/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 01/02/2019  12:50 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "Core.h"

#include "LogSystem/LogSystem.h"
#include "Library/EngineLibrary.h"
#include "Parsers/EngineIniParser.h"
#include "EngineEventListener.h"
#include <UnkownLibrary/IUnknownLibrary.h>

#include "Console/EngineConsole.h"

void QuitFunction(IConsoleFuncArgs* pArgs)
{
	gEngine->pCore->Quit();
}

CEngineCore::CEngineCore()
	: m_pLogSystem(nullptr)
	, m_rdRenderNullLibrary(nullptr)
	, m_pRenderNull(nullptr)
	, m_bIsInitComplete(false)
	, m_pInitParams(nullptr)
	, m_bIsCoreLoop(false)
	, m_pEngineConsole(nullptr)
{
}

CEngineCore::~CEngineCore()
{
	SAFE_DELETE(m_pLogSystem);
	SAFE_DELETE(m_pEngineLibrary);
}

void CEngineCore::Initialize(SInitializeParams& params)
{
	m_pInitParams = &params;
	gEngine = new ICoreGlobalVariable(); // Intializing global engine var;

	params.pCore = this;
	gEngine->pCore = params.pCore;

	TODO("DO INITALIZE ALL ENGINE IN FUTURE HERE...");

	m_rootDir = params.rootDir;

	m_pLogSystem = new CLogSystem();
	m_pEngineIniParser = new CEngineIniParser();

	params.pLogSystem = m_pLogSystem;
	gEngine->pLog = params.pLogSystem;

	m_pLogSystem->SetLogFileName(params.logFileName);
	m_pLogSystem->Initialize();

	m_pEngineConsole = new CEngineConsole();
	gEngine->pConsole = m_pEngineConsole;

	m_pEngineEvent = new CEngineEvent();
	
	CoreLog("Initializing Engine...");
	GetIEngineEvent()->EngineEvent(EEngineEvents::EVENT_ENGINE_INIT);
	gEngine->pConsole->RegisterFunction("Quit", &QuitFunction);

	LoadEngineLibraries(params);
	
	TODO("Initalizing classes...");


	m_bIsInitComplete = true;
}

bool CEngineCore::CompleteInit()
{
	if (m_bIsInitComplete)
	{
		
	}

	return m_bIsInitComplete;
}

void CEngineCore::Release()
{
	CoreLog("Releasing Engine Unknown Library's...");
	m_pEngineLibrary->Release();

	this->~CEngineCore();
}

ICoreGlobalVariable * CEngineCore::GetGlobalVariable()
{
	return gEngine;
}


void CEngineCore::RunCoreLoop()
{
	m_bIsCoreLoop = true;
	CoreLoop(0);
}

void CEngineCore::Quit()
{
	m_bIsCoreLoop = false;
}

void CEngineCore::IsQuiting()
{
}

ILogSystem * CEngineCore::GetILogSystem()
{
	return reinterpret_cast<ILogSystem*>(m_pLogSystem);;
}

IEngineLibrary * CEngineCore::GetIIEngineLibrary()
{
	return reinterpret_cast<IEngineLibrary*>(m_pEngineLibrary);
}

IEngineIniParser * CEngineCore::GetIEngineIniParser()
{
	return reinterpret_cast<IEngineIniParser*>(m_pEngineIniParser);
}

IEngineEvent * CEngineCore::GetIEngineEvent()
{
	return reinterpret_cast<IEngineEvent*>(m_pEngineEvent);
}

void CEngineCore::LoadEngineLibraries(SInitializeParams& params)
{
	m_pEngineLibrary = new CEngineLibrary();

	const string SECTION = "EngineLibraries";
	IInih* pEngineLibSection = GetIEngineIniParser()->LoadFile("engine/libraries.ini");

	if (pEngineLibSection->GetParsError() == -1)
		FATAL_ERROR("Engine Library .ini file not found!");

	std::set<string> engineLibs = pEngineLibSection->GetFields(SECTION);

	for (auto& lib : engineLibs)
	{
		string libPath = pEngineLibSection->GetString(SECTION, lib, "null");
		m_pEngineLibrary->LoadUnknownLibrary(libPath.c_str(), params);
	}

	//All libs loaded...
	GetIEngineEvent()->EngineEvent(EEngineEvents::EVENT_ENGINE_LIBS_LOADED);
}

void CEngineCore::CoreLoop(float frameTime)
{
	while (m_bIsCoreLoop)
	{
		for (auto& lib : m_pEngineLibrary->GetUnknownLibs())
		{
			lib.m_pEngineLibraries->OnUpdate();
		}
	}
}
