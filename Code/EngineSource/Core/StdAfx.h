/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 22/12/2018  20:39 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _CORE_STDAFX_H__
#define _CORE_STDAFX_H__

#define USE_STD_VECTOR
#define ENGINE_API_EXPORT

#include <Engine/enginecore.h>
#include <Core/ILogSystem.h>

#define MODULE_TITILE "[Core] "

#define CoreLog(...) do {  gEngine->pLog->Log(MODULE_TITILE __VA_ARGS__); } while(0)
#define CoreLogWarning(...) do {  gEngine->pLog->LogWarning(MODULE_TITILE __VA_ARGS__); } while(0)
#define CoreLogError(...) do {  gEngine->pLog->LogError(MODULE_TITILE __VA_ARGS__); } while(0)
#define CoreLogSuccess(...) do {  gEngine->pLog->LogSuccess(MODULE_TITILE __VA_ARGS__); } while(0)

#define FATAL_ERROR(x) { RadMessageBox("FATAL ERROR", x, eBT_Error); }

#endif