/*************************************************************************
Copyright (C), RADFOX Dev Team, 2018-2019.
-------------------------------------------------------------------------
History:
- 26/12/2018  03:39 : Created by Stanislav Migunov
- 20/01/2019  21:41 : Last edit by Stanislav Migunov
*************************************************************************/


#include "Engine/enginecore.h"
#include "Engine/enginecore_impl.inl"
#include "RFLibrary.h"

RadDynamic rdCoreDll = nullptr;

int RunGame(const char* commandLine)
{
	//if (std::strstr(commandLine, "-con_debug")) 
#if defined(_DEBUG)
	OpenLogConsole();
#endif

	rdCoreDll = RadLoadLibrary(Library_Core);
	if (!rdCoreDll)
	{
		MessageBox(0, "Failed to load the Core DLL!", "Error", MB_OK | MB_ICONERROR);
		return 0;
	}
	IEngineCore::TEntryPoint epCreateCoreStartup = RadProcAddress<IEngineCore::TEntryPoint>(rdCoreDll, "CreateCoreDllStartup");
	if (!epCreateCoreStartup)
	{
		RadLibraryFree(rdCoreDll);
		MessageBox(0, "Core Startup creation failed!", "Error", MB_OK | MB_ICONERROR);
		return 0;
	}

	IEngineCore* pEngineCore = epCreateCoreStartup();
	if (!pEngineCore)
	{
		RadLibraryFree(rdCoreDll);
		MessageBox(0, "Creation of the Core Interface failed", "Error", MB_OK | MB_ICONERROR);
		return 0;
	}

	InitRootDir();

	char dirBuffer[256];
	GetCurrentDirectory(sizeof(dirBuffer), dirBuffer);

	SInitializeParams initParams;
	initParams.rootDir = dirBuffer;
	initParams.logFileName = "Engine";

	pEngineCore->Initialize(initParams);

	if (pEngineCore->CompleteInit())
	{
		//Create CoreLoop;
		pEngineCore->RunCoreLoop();

		pEngineCore->Release();
	}
	else
	{
		MessageBox(0, "Core Startup failed!", "Error", MB_OK | MB_ICONERROR);
		pEngineCore->Release();
		RadLibraryFree(rdCoreDll);
		return 0;
	}

	return 0;
}


int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int mCmdShow)
{
	return RunGame(lpCmdLine);
}