cmake_minimum_required (VERSION 3.0)

project("RADFOX Engine")

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -D_DEBUG")

include_directories(Code/SDK)

set(ENGINE_ROOT ${CMAKE_CURRENT_SOURCE_DIR})

set(CMAKE_BINARY_DIR ${ENGINE_ROOT}/_x64_)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})

add_subdirectory(Code/EngineSource/Core)
add_subdirectory(Code/GameDll)
add_subdirectory(Code/EngineSource/Input)
add_subdirectory(Code/EngineSource/RenderNull)
add_subdirectory(Code/EngineSource/RenderVulkan)
add_subdirectory(Code/EngineSource/Common)
add_subdirectory(Code/EngineSource/GLFW)

if(WIN32)
    add_subdirectory(Code/Launcher)
elseif(UNIX)
    add_subdirectory(Code/LinuxLauncher)
endif()

